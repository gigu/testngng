package functional.org.testngng.bar;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 19/07/2012
 */
public class FunctionalTestFoo {

    @Test
    public void someAwesomeFunctionalTest() {
        System.out.println("This should be intercepted too, wohooooo");
        System.out.print("One more for the winner");
        assertTrue(true);
    }

    @Test
    public void thisOneIsAFailingTest(){
        System.out.println("Hope this will be intercepted at some point");
        assertTrue(false);
    }
}

