package unit.org.testngng.foo;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.InvalidClassException;

import static org.testng.Assert.assertTrue;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 19/07/2012
 */
public class BarTest {

    @Test
    public void awesomePassingTest(){
        System.out.println("Dupa Jasna holera");
        assertTrue(true);
    }

    @Test(expectedExceptions = ClassCastException.class)
    public void willFail() throws InvalidClassException {
        System.out.println("Dupa dupa");
        System.err.print("Holera jasna");

        throw new InvalidClassException("Some class");
    }

    @BeforeTest
    public void doStuff() throws InterruptedException {
        System.out.println("Dupa sraka");
        Thread.sleep(10);
    }
}
