package unit.org.testngng.foo;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testngng.foo.FooStuff;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 09/07/2012
 */
public class FooStuffTest {

    @Test
    public void testAndIReturnTrue() throws Exception {
        assertTrue(new FooStuff().andIReturnTrue());
    }

    @Test
    public void testAndIReturnFalse() throws Exception {
        assertFalse(new FooStuff().andIReturnFalse());
    }

    @Test
    public void testAndINegateWhateva() throws Exception {
        assertFalse(new FooStuff().andINegateWhateva(true));
        assertTrue(new FooStuff().andINegateWhateva(false));
    }

    @Test(dataProvider = "makeMeSomeData")
    public void testWithProvider(String v1, String v2){
        System.out.println(String.format("%s - %s ", v1, v2));
    }

    @DataProvider
    public Object[][] makeMeSomeData() {
        return new Object[][]{
                {"some1", "Some2"},
                {"some3", "some4"}
        };
    }

}
