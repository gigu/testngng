package org.testngng.foo;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 09/07/2012
 */
public class FooStuff {
    public String iDontHaveMuchInMe;

    public boolean andIReturnTrue() {
        return true;
    }

    public boolean andIReturnFalse() {
        return false;
    }

    public boolean andINegateWhateva(boolean whateva) {
        return !whateva;
    }
}
