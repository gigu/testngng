package org.testngng.exceptions

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 22/05/2012
 */
class TestInitializationException extends Exception {
    def TestInitializationException(String message) {
        super(message)
    }

    def TestInitializationException(String message, Exception ex) {
        super(message, ex)
    }
}
