package org.testngng.assemblers

import org.testngng.domain.TestClass
import org.testngng.domain.TestSuite
import org.testngng.finders.Methods
import org.testngng.finders.TestSetupMethodsFinder

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 29/05/2012
 */
class SuiteAssembler {
    TestSetupMethodsFinder testSetupMethodsFinder
    TestClassAssembler testClassAssembler

    TestSuite assembleSuite(List<Class> classes, def name = TestSuite.DEFAULT_NAME) {
        def beforeSuite = new HashSet(), afterSuite = new HashSet()
        def testClasses = []
        classes.each { Class aClass ->
            def instance = TestClass.createInstance(aClass)
            def setupMethods = testSetupMethodsFinder.findMeSomeMethods(instance)

            def testClass = testClassAssembler.assemble(aClass, setupMethods, instance)
            if (testClass.testMethods.size() > 0) {
                testClasses << testClass

                addUnique(setupMethods, beforeSuite, Methods.BEFORE_SUITE)
                addUnique(setupMethods, afterSuite, Methods.AFTER_SUITE)
            }
        }
        new TestSuite(
                testClasses: testClasses,
                beforeSuite: beforeSuite.asList(),
                afterSuite: afterSuite.asList(),
                name: name
        )
    }

    private def addUnique(def setupMethods, def methodsList, def methodType) {
        methodsList.addAll setupMethodsFor(setupMethods, methodType)
    }

    private def setupMethodsFor(setupMethods, methodType) {
        setupMethods[methodType] == null ? [] : setupMethods[methodType]
    }

}
