package org.testngng.assemblers

import org.testngng.converters.AnnotatedMethodToTestMethodConverter
import org.testngng.domain.TestClass
import org.testngng.finders.Methods
import org.testngng.finders.TestAnnotationFinder

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 21/05/2012
 */
class TestClassAssembler {
    AnnotatedMethodToTestMethodConverter methodConverter
    TestAnnotationFinder testAnnotationFinder

    TestClass assemble(Class clazz, Map setupMethods, def instance) {
        def testClass = new TestClass(name: clazz.name, instance: instance)

        testClass.beforeClassMethods = setupMethods[Methods.BEFORE_CLASS]
        testClass.afterClassMethods = setupMethods[Methods.AFTER_CLASS]

        def classAnnotationResults = testAnnotationFinder.findMeSomeTestsAndStuffInClass(clazz)

        testClass.testMethods = []
        classAnnotationResults.testMethods.each { def method ->
            testClass.testMethods << methodConverter.pleaseConvert(method, setupMethods)
        }

        testClass
    }
}
