package org.testngng.scanner

import java.lang.reflect.Modifier

import static org.testngng.utils.HappyBunchOfHelpers.isNullOrEmpty

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 13/06/2012
 */
class FolderScanner {

    def scan(File folder, def includesFolders = []) {
        def listOfClasses = []
        if (includesFolders.size() > 0) {
            includesFolders.each { listOfClasses.addAll scanFolder(new File(folder, it), it) }
        } else {
            listOfClasses = scanFolder(folder, "")
        }
        def classes = []
        listOfClasses.each { className ->
            try {
                def classOnAPath = Class.forName(className)
                if (!Modifier.isAbstract(classOnAPath.modifiers) && Modifier.isPublic(classOnAPath.modifiers)) {
                    classes << classOnAPath
                }
            } catch (ClassNotFoundException) {}
        }
        classes
    }

    private def scanFolder(File folder, String packageName) {
        def files = []
        folder.eachFile { File file ->
            if (file.isDirectory()) {
                files.addAll scanFolder(file, isNullOrEmpty(packageName) ? file.name : "${packageName}.${file.name}")
            }
            if (file.name.endsWith(".class")) {
                files << classWithPackage(file, packageName)
            }
        }
        files
    }

    private String classWithPackage(File file, String packageName) {
        def className = file.name.substring(0, file.name.length() - 6)
        isNullOrEmpty(packageName) ? className : "${packageName}.${className}"
    }
}
