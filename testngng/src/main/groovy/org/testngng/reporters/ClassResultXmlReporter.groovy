package org.testngng.reporters

import org.testngng.domain.ClassResult
import groovy.xml.MarkupBuilder
import org.testngng.domain.TestResult

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 10/07/2012
 */
class ClassResultXmlReporter {
    File reportsFolder

    public void createReportFileForClassResult(ClassResult classResult) {
        def file = new File(reportsFolder, fileNameForClass(classResult))
        file.createNewFile()
        def fileWriter = new FileWriter(file)
        def xml = new MarkupBuilder(fileWriter)
        xml.testsuite(name: classResult.className, tests: classResult.testResults.size(), time: '', timestamp: classResult.timestamp) {
            'properties'()
            classResult.testResults.each { TestResult testResult ->
                if (testResult == TestResult.FAILURE) {
                    testcase(classname: classResult.className, name: testResult.testName, time: '') {
                        failure(message: testResult.message, type: testResult.exception.class.name, exceptionMessage(testResult.exception))
                    }
                } else {
                    testcase(classname: classResult.className, name: testResult.testName, time: '')
                }

            }

        }
    }

    private String fileNameForClass(ClassResult classResult) {
        return "TEST-${classResult.className}.xml"
    }

    private def exceptionMessage(def exception) {
        if (!exception) return ""
        "${exception}\n  ${exception.stackTrace.join('\n  ')}"
    }
}
