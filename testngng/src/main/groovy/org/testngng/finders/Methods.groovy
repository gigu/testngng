package org.testngng.finders

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 21/05/2012
 */
class Methods {
    static String BEFORE_TEST = "BEFORE_TEST"
    static String BEFORE_METHOD = "BEFORE_METHOD"
    static String BEFORE_SUITE = "BEFORE_SUITE"
    static String BEFORE_CLASS = "BEFORE_CLASS"
    static String AFTER_TEST = "AFTER_TEST"
    static String AFTER_METHOD = "AFTER_METHOD"
    static String AFTER_SUITE = "AFTER_SUITE"
    static String AFTER_CLASS = "AFTER_CLASS"

    static String DATA_PROVIDER = "DATA_PROVIDER"

    static def ALL_METHODS = [BEFORE_TEST, BEFORE_METHOD, BEFORE_CLASS, BEFORE_SUITE, AFTER_TEST, AFTER_METHOD, AFTER_CLASS, AFTER_SUITE, DATA_PROVIDER]

    static def SUITE_SETUP_METHODS = [BEFORE_SUITE, AFTER_SUITE]
    static def CLASS_SETUP_METHODS = [BEFORE_CLASS, AFTER_CLASS]

    static def TEST_SETUP_METHODS = [BEFORE_TEST, BEFORE_METHOD, AFTER_TEST, AFTER_METHOD, DATA_PROVIDER]

}
