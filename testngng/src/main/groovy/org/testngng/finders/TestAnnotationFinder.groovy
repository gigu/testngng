package org.testngng.finders

import org.testngng.domain.ClassAnnotationResults

import java.lang.annotation.Annotation
import java.lang.reflect.Method
import java.lang.reflect.Modifier

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 15/05/2012
 */
class TestAnnotationFinder {
    def findMeSomeTestsAndStuffInClass(Class clazz) {
        if (classIsAnnotated(clazz)) {
            return allTestMethodsInAnnotatedClass(clazz)
        }
        return allTestMethodsFromAnnotatedMethods(clazz)
    }

    private def allTestMethodsFromAnnotatedMethods(Class clazz) {
        def methods = []
        clazz.declaredMethods.each { Method method ->
            def annotations = method.declaredAnnotations
            if (annotations.size() != 0) {
                def testAnnotation = annotations.find { it.annotationType() == TestNGAnnotationClasses.TEST_CLASS }
                if (testAnnotation) {
                    methods << annotatedMethod(method, testAnnotation)
                }
            }
        }
        new ClassAnnotationResults(testMethods: methods)
    }

    private LinkedHashMap annotatedMethod(def method, def testAnnotation) {
        def annotatedMethod = [:]
        annotatedMethod[ClassAnnotationResults.METHOD] = method
        annotatedMethod[ClassAnnotationResults.ANNOTATION] = testAnnotation
        return annotatedMethod
    }

    private ClassAnnotationResults allTestMethodsInAnnotatedClass(Class clazz) {
        def testAnnotation = clazz.annotations.find { it.annotationType() == TestNGAnnotationClasses.TEST_CLASS }
        def testMethods = []
        clazz.declaredMethods.each { Method method ->
            if (Modifier.isPublic(method.modifiers) && !Modifier.isAbstract(method.modifiers) && notATestSetupMethod(method)){
                testMethods << annotatedMethod(method, testAnnotation)
            }
        }
        return new ClassAnnotationResults(testMethods: testMethods)
    }

    private Boolean notATestSetupMethod(Method method) {
        !method.annotations.any { Annotation annotation -> TestNGAnnotationClasses.SETUP_METHODS.find { annotation.annotationType()} }
    }

    private boolean classIsAnnotated(Class aClass) {
        aClass.annotations.length > 0 && aClass.annotations.find { it.annotationType() == TestNGAnnotationClasses.TEST_CLASS }
    }


}
