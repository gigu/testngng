package org.testngng.finders

import org.testngng.domain.DataProvider
import org.testngng.domain.SuiteSetupMethod

import java.lang.annotation.Annotation
import java.lang.reflect.Method

import static org.testngng.utils.HappyBunchOfHelpers.isNullOrEmpty

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 21/05/2012
 */
class TestSetupMethodsFinder {

    def findMeSomeMethods(def instance) {
        def aClass = instance.class
        def methods = [:]
        def declaredMethods = aClass.declaredMethods
        def allMethods = aClass.methods

        methods[Methods.BEFORE_METHOD] = findAllMethodNamesAnnotatedWith(declaredMethods, TestNGAnnotationClasses.BEFORE_METHOD)
        methods[Methods.BEFORE_TEST] = findAllMethodNamesAnnotatedWith(declaredMethods, TestNGAnnotationClasses.BEFORE_TEST)
        methods[Methods.AFTER_METHOD] = findAllMethodNamesAnnotatedWith(declaredMethods, TestNGAnnotationClasses.AFTER_METHOD)
        methods[Methods.AFTER_TEST] = findAllMethodNamesAnnotatedWith(declaredMethods, TestNGAnnotationClasses.AFTER_TEST)

        methods[Methods.BEFORE_CLASS] = findAllMethodNamesAnnotatedWith(allMethods, TestNGAnnotationClasses.BEFORE_CLASS)
        methods[Methods.AFTER_CLASS] = findAllMethodNamesAnnotatedWith(allMethods, TestNGAnnotationClasses.AFTER_CLASS)

        methods[Methods.BEFORE_SUITE] = findAllSuiteMethodsAnnotatedWith(allMethods, TestNGAnnotationClasses.BEFORE_SUITE, instance)
        methods[Methods.AFTER_SUITE] = findAllSuiteMethodsAnnotatedWith(allMethods, TestNGAnnotationClasses.AFTER_SUITE, instance)

        methods[Methods.DATA_PROVIDER] = findAllDataProviderMethods(declaredMethods)
        methods
    }

    private def findAllDataProviderMethods(def methods) {
        def foundMethods = findAnnotatedMethods(methods, TestNGAnnotationClasses.DATA_PROVIDER)
        def providers = [:]
        foundMethods.each { method ->
            def dataProviderAnnotation = method.annotations.find { Annotation annotation -> annotation.annotationType() == TestNGAnnotationClasses.DATA_PROVIDER }
            def name = isNullOrEmpty(dataProviderAnnotation.name()) ? method.name : dataProviderAnnotation.name()
            providers[name] = new DataProvider(name: name, methodName: method.name)
        }
        providers
    }

    private def findAllMethodNamesAnnotatedWith(def methods, Class annotationType) {
        def foundMethods = findAnnotatedMethods(methods, annotationType)
        foundMethods.collect { it.name }
    }

    private def findAllSuiteMethodsAnnotatedWith(def methods, Class annotationType, def instance) {
        def foundMethods = findAnnotatedMethods(methods, annotationType)
        foundMethods.collect { new SuiteSetupMethod(name: it.name, declaringClass: it.declaringClass, instance: instance) }
    }

    private Collection findAnnotatedMethods(methods, Class annotationType) {
        return methods.findAll { Method method ->
            def annotated = method.annotations.find { Annotation annotation -> annotation.annotationType() == annotationType}
            annotated
        }
    }
}

