package org.testngng.finders

import org.testng.annotations.*

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 21/05/2012
 */
class TestNGAnnotationClasses {
    static Class TEST_CLASS = Test.class

    static Class BEFORE_METHOD = BeforeMethod.class
    static Class BEFORE_TEST = BeforeTest.class
    static Class BEFORE_CLASS = BeforeClass.class
    static Class BEFORE_SUITE = BeforeSuite.class

    static Class AFTER_METHOD = AfterMethod.class
    static Class AFTER_TEST = AfterTest.class
    static Class AFTER_CLASS = AfterClass.class
    static Class AFTER_SUITE = AfterSuite.class

    static Class DATA_PROVIDER = DataProvider.class

    static Class[] SETUP_METHODS = [
            BEFORE_CLASS, BEFORE_METHOD, BEFORE_SUITE, BEFORE_TEST,
            AFTER_CLASS, AFTER_METHOD, AFTER_SUITE, AFTER_TEST,
            DATA_PROVIDER
    ]
}
