package org.testngng

import org.testngng.executors.TestNGNGExecutorsFactory
import org.testngng.scanner.FolderScanner
import org.testngng.utils.Logger
import org.testngng.domain.Result

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 13/06/2012
 */
class TestNGNG {
    public static int SUCCESS = 0
    public static int FAILURE = -1

    private File folderWithTestClasses
    private File xmlDestination
    private File htmlDestination

    private String suiteName
    private String[] includes

    def static final String USAGE = "Run TestNGNG with two parameters: folder-with-test-class-files xml-reports-destination-folder html-reports-destination-folder suite-name includes\n All parameters are mandatory."

    public static void main(String[] args) {
        if (args.length < 4) {
            println USAGE
            System.exit(FAILURE);
        }
        def result = new TestNGNG(folderWithTestClasses: new File(args[0]), xmlDestination: new File(args[1]), htmlDestination: new File(args[2]), suiteName: args[3], includes: getListOfIncludes(args)).run()
        if (result.suiteResult == Result.SUCCESS) {
            System.exit(SUCCESS)
        }
        System.exit(FAILURE)
    }

    private static def getListOfIncludes(String[] args) {
        if (args.length < 5){
            return []
        }
        args[4].contains(';') ? args[4].split(';') : [args[4]]
    }

    void run() {
        Logger.initiateStreams()
        if (!xmlDestination.exists()) {
            xmlDestination.mkdirs()
        }
        if (!htmlDestination.exists()) {
            htmlDestination.mkdirs()
        }
        def executor = new TestNGNGExecutorsFactory().executor(xmlDestination, htmlDestination, suiteName)

        def classes = new FolderScanner().scan(folderWithTestClasses, includes)

        executor.execute(classes, suiteName)
    }

}
