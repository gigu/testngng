package org.testngng.executors

import org.testngng.assemblers.SuiteAssembler
import org.testngng.assemblers.TestClassAssembler
import org.testngng.converters.AnnotatedMethodToTestMethodConverter
import org.testngng.finders.TestAnnotationFinder
import org.testngng.finders.TestSetupMethodsFinder
import org.testngng.runners.ClassRunner
import org.testngng.runners.SuiteRunner
import org.testngng.runners.TestMethodsSorter
import org.testngng.runners.TestRunner
import org.testngng.reporters.ClassResultXmlReporter

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
class TestNGNGExecutorsFactory {
    def executor(File xmlReportsFolder, File htmlReportsFolder, String suiteName = "DefaultSuite") {
        def simpleTestNGNGExecutor = simpleTestNGNGExecutor()

        if (!xmlReportsFolder && !htmlReportsFolder) {
            return simpleTestNGNGExecutor
        }

        if (xmlReportsFolder && !htmlReportsFolder) {
            return xmlReportingTestNGNGExecutor(xmlReportsFolder)
        }

        if (!xmlReportsFolder && htmlReportsFolder) {
            return new HtmlReportingTestNGNGExecutor(testNGNGExecutor: simpleTestNGNGExecutor, htmlDestinationFolder: htmlReportsFolder)
        }
        new HtmlReportingTestNGNGExecutor(testNGNGExecutor: xmlReportingTestNGNGExecutor(xmlReportsFolder), htmlDestinationFolder: htmlReportsFolder)
    }

    private XmlReportingTestNGNGExecutor xmlReportingTestNGNGExecutor(File reportsFolder) {
        def simpleTestNGNGExecutor = new SimpleTestNGNGExecutor(suiteRunner: suiteRunner(reportsFolder), suiteAssembler: suiteAssembler())
        new XmlReportingTestNGNGExecutor(simpleTestNGNGExecutor: simpleTestNGNGExecutor, reportsFolder: reportsFolder)
    }

    private SimpleTestNGNGExecutor simpleTestNGNGExecutor() {
        new SimpleTestNGNGExecutor(suiteRunner: suiteRunner(), suiteAssembler: suiteAssembler())
    }

    private SuiteAssembler suiteAssembler() {
        new SuiteAssembler(
                testClassAssembler: new TestClassAssembler(
                        methodConverter: new AnnotatedMethodToTestMethodConverter(),
                        testAnnotationFinder: new TestAnnotationFinder()
                ),
                testSetupMethodsFinder: new TestSetupMethodsFinder()
        )
    }

    private SuiteRunner suiteRunner(File reportsFolder = null) {
        if (reportsFolder != null) {
            return new SuiteRunner(classRunner: classRunner(new ClassResultXmlReporter(reportsFolder: reportsFolder)))
        }
        new SuiteRunner(classRunner: classRunner())
    }

    private ClassRunner classRunner(def classResultXmlReporter = null) {
        return new ClassRunner(
                classResultXmlReporter: classResultXmlReporter,
                singleTestRunner: new TestRunner(),
                testMethodsSorter: new TestMethodsSorter()
        )
    }
}
