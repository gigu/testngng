package org.testngng.executors

import org.testngng.exceptions.TestInitializationException

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 12/06/2012
 */
class XmlReportingTestNGNGExecutor {
    SimpleTestNGNGExecutor simpleTestNGNGExecutor
    File reportsFolder

    def execute(List classesList, String suitName = "DefaultSuite") {
        if (!reportsFolder) {
            throw new TestInitializationException("XML test reports folder is not set")
        }
        createReportsFolderIfNotExists()

        simpleTestNGNGExecutor.execute(classesList, suitName)
    }

    private def createReportsFolderIfNotExists() {
        if (!reportsFolder.exists()) reportsFolder.mkdirs()
    }
}
