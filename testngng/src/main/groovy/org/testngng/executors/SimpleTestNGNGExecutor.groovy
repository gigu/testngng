package org.testngng.executors

import org.testngng.assemblers.SuiteAssembler
import org.testngng.domain.SuiteResult
import org.testngng.runners.SuiteRunner

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 04/06/2012
 */
class SimpleTestNGNGExecutor {
    SuiteAssembler suiteAssembler
    SuiteRunner suiteRunner

    SuiteResult execute(List classList, String suiteName = "DefaultSuite") {
        def suite = suiteAssembler.assembleSuite(classList, suiteName)
        suiteRunner.run(suite)
    }
}
