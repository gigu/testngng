package org.testngng.executors

import groovy.text.GStringTemplateEngine
import org.testngng.domain.ClassResult
import org.testngng.domain.SuiteResult
import org.testngng.exceptions.TestInitializationException
import org.testngng.utils.HappyBunchOfHelpers

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 25/06/2012
 */
class HtmlReportingTestNGNGExecutor {
    private GStringTemplateEngine engine = new GStringTemplateEngine()
    private String reportsTime = new Date().toString()

    File htmlDestinationFolder
    def testNGNGExecutor

    static def TEMPLATE_BASE = '/html-report-template/'
    static def STATIC_FILES_BASE = TEMPLATE_BASE + 'static/'

    static def SUITE_TEMPLATE = 'suite.html.template'
    static def PACKAGE_TEMPLATE = 'package.html.template'
    static def CLASS_TEMPLATE = 'class.html.template'

    static def STATIC_FILES = [
            'base-style.css',
            'style.css',
            'report.js',
    ]

    def execute(List classesList, String suiteName = "DefaultSuite") {
        if (!htmlDestinationFolder) {
            throw new TestInitializationException("The reports folder for HTML is not set")
        }
        def suiteResult = testNGNGExecutor.execute(classesList, suiteName)
        createHtmlReport(suiteResult)
        suiteResult
    }

    private def createHtmlReport(SuiteResult suiteResult) {
        copyStaticFiles()

        def groupedResults = groupResultsByPackage(suiteResult)
        def packagesAndSuiteSummary = preparePackageAndSuiteSummary(groupedResults, suiteResult)

        generateSuitePage(groupedResults, suiteResult, packagesAndSuiteSummary.packageSummary, packagesAndSuiteSummary.suiteSummary)
        generatePackageReport(groupedResults, suiteResult, packagesAndSuiteSummary.packageSummary)
        generateClassReport(groupedResults, suiteResult)
    }

    private def generateClassReport(def groupedResults, SuiteResult suiteResult) {
        def template = engine.createTemplate(classFile)
        suiteResult.classResults.each { ClassResult classResult ->
            def outputFile = new File(htmlDestinationFolder, "${classResult.className}.html")
            def binding = template.make([classResult: classResult, time: reportsTime, suiteResult: suiteResult])
            outputFile.text = binding.toString()
        }
    }

    private def generatePackageReport(def groupedResults, SuiteResult suiteResult, def packagesSummary) {
        def template = engine.createTemplate(packageFile)
        groupedResults.each { entry ->
            def packageName = entry.key
            def binding = template.make([suiteResult: suiteResult, packageName: packageName, packageSummary: packagesSummary["$packageName"], classesSummary: entry.value, time: reportsTime])

            def outputFile = new File(htmlDestinationFolder, "${packageName}.html")
            outputFile.text = binding.toString()
        }
    }

    private def generateSuitePage(def groupedResults, SuiteResult suiteResult, def packagesSummary, def suiteSummary) {
        def template = engine.createTemplate(suiteFile)
        def binding = template.make([suiteSummary: suiteSummary, suiteResult: suiteResult, packagesSummary: packagesSummary, time: reportsTime])
        def outputFile = new File(htmlDestinationFolder, "suite-${suiteResult.suiteName}.html")
        outputFile.text = binding.toString()
    }

    private def preparePackageAndSuiteSummary(def groupedResults, def suiteResult) {
        def packagesSummary = [:]
        def suiteSummary = ['totalTests': 0, 'totalFailures': 0, 'totalTime': formatDuration(suiteResult.time)]

        groupedResults.each { mapEntry ->
            packagesSummary[mapEntry.key] = [:]
            packagesSummary[mapEntry.key]['tests'] = mapEntry.value.inject(0) { initialValue, classResult -> initialValue + classResult.testResults.size() }
            packagesSummary[mapEntry.key]['failures'] = mapEntry.value.inject(0) { initialValue, classResult -> initialValue + classResult.failures }
            packagesSummary[mapEntry.key]['duration'] = formatDuration(mapEntry.value.inject(0) { initialValue, classResult -> initialValue + classResult.time })
            packagesSummary[mapEntry.key]['successRate'] = roundedPercentage(packagesSummary[mapEntry.key]['tests'], packagesSummary[mapEntry.key]['failures'])
            packagesSummary[mapEntry.key]['style'] = (packagesSummary[mapEntry.key]['failures'] > 0) ? 'failures' : 'success'

            suiteSummary['totalTests'] += packagesSummary[mapEntry.key]['tests']
            suiteSummary['totalFailures'] += packagesSummary[mapEntry.key]['failures']
        }
        suiteSummary['successRate'] = roundedPercentage(suiteSummary.totalTests, suiteSummary.totalFailures)
        suiteSummary['style'] = (suiteSummary.totalFailures > 0) ? 'failures' : 'success'
        [packageSummary: packagesSummary, suiteSummary: suiteSummary]
    }

    private def roundedPercentage(def numberOfTests, def failures) {
        (failures > 0) ? Math.floor((numberOfTests - failures) / numberOfTests * 100) : 100
    }

    private def copyStaticFiles() {
        STATIC_FILES.each { fileName ->
            def inputStream = this.class.getResourceAsStream(STATIC_FILES_BASE + fileName)
            HappyBunchOfHelpers.copyStreamIntoFile(inputStream, new File(htmlDestinationFolder, fileName))
        }
    }

    private def groupResultsByPackage(SuiteResult result) {
        result.classResults.groupBy { ClassResult classResult -> classResult.package }
    }

    private def getSuiteFile() {
        new InputStreamReader(this.class.getResourceAsStream(TEMPLATE_BASE + SUITE_TEMPLATE))
    }

    private def getPackageFile() {
        new InputStreamReader(this.class.getResourceAsStream(TEMPLATE_BASE + PACKAGE_TEMPLATE))
    }

    private def getClassFile() {
        new InputStreamReader(this.class.getResourceAsStream(TEMPLATE_BASE + CLASS_TEMPLATE))
    }

    private def formatDuration(def time) {
        (time > 100) ? "${time / 100 } s" : "${time} ms"
    }

}
