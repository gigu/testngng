package org.testngng.runners

import org.testngng.domain.ClassResult
import org.testngng.domain.Result
import org.testngng.domain.TestResult

import static org.testngng.utils.HappyBunchOfHelpers.timeTakenFrom
import org.testngng.utils.Logger

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 22/05/2012
 */
class ClassRunner {
    def singleTestRunner
    def testMethodsSorter
    def classResultXmlReporter

    def run(def testClass) {
        Logger.startIntercepting()
        def start = System.currentTimeMillis()
        def testResults, classResult
        try {
            testClass.beforeClassMethods.each { testClass.instance."$it"() }
        } catch (exception) {
            Logger.errorln("Test ${testClass.name} has FAILED")
            return new ClassResult(classResult: Result.FAILURE, className: testClass.name, timestamp: start, stdOutErr: Logger.stopIntercepting())
        }
        if (classContainsTestsWithDependencies(testClass.testMethods)) {
            testResults = executeTests(testMethodsSorter.sort(testClass.testMethods), testClass.instance)
        } else {
            testResults = executeTests(testClass.testMethods, testClass.instance)
        }

        testClass.afterClassMethods.each { testClass.instance."$it"() }

        def result = result(testResults, testClass.name)
        classResult = new ClassResult(testResults: testResults, classResult: result, className: testClass.name, time: timeTakenFrom(start), timestamp: start, stdOutErr: Logger.stopIntercepting())

        if (classResultXmlReporter) {
            classResultXmlReporter.createReportFileForClassResult(classResult)
        }

        classResult
    }

    private boolean classContainsTestsWithDependencies(def listOfMethods) {
        listOfMethods.find { it.dependsOn != null } != null
    }

    private def executeTests(def methodsToExecute, def instance) {
        def results = []
        methodsToExecute.each { results.addAll singleTestRunner.run(instance, it) }
        results
    }

    private def result(def testResults, def className) {
        if (testResults.find { it == TestResult.FAILURE }) {
            Logger.out('F')
            Logger.errorln("Test ${className} has FAILED")
            return Result.FAILURE
        }
        Logger.out('.')
        Result.SUCCESS
    }
}
