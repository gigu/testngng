package org.testngng.runners

import org.testngng.domain.Result
import org.testngng.domain.SuiteResult

import static org.testngng.utils.HappyBunchOfHelpers.timeTakenFrom
import org.testngng.utils.Logger

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 23/05/2012
 */
class SuiteRunner {
    def classRunner

    def run(def testSuite) {
        def start = System.currentTimeMillis()
        def classResults = []
        try {
            testSuite.beforeSuite.each { it.instance."$it.name"() }
        } catch (exception) {
            Logger.errorln(exception.message)
            return new SuiteResult(classResults: [], suiteName: testSuite.name, suiteResult: Result.SKIPPED, time: 0)
        }
        testSuite.testClasses.each { classResults << classRunner.run(it) }

        testSuite.afterSuite.each { it.instance."$it.name"() }

        new SuiteResult(
                classResults: classResults,
                suiteName: testSuite.name,
                suiteResult: suiteResult(classResults),
                time: timeTakenFrom(start)
        )
    }

    private def suiteResult(def classResultList) {
        if (classResultList.find { it.classResult == Result.FAILURE}) {
            return Result.FAILURE
        }
        Result.SUCCESS
    }
}
