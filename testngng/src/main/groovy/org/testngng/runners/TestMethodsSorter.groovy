package org.testngng.runners

import org.testngng.exceptions.TestInitializationException

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
class TestMethodsSorter {

    def sort(def methodsList) {
        if (thereAreMethodsWithDependenciesIn(methodsList)) {
            def allTheMethodsWithNoDependencies = methodsWithNoDependencies(methodsList)
            def methodsWithDependencies = methodsList - allTheMethodsWithNoDependencies
            return methodsWithDependenciesInOrder(allTheMethodsWithNoDependencies, methodsWithDependencies)
        }
        methodsList
    }

    private def methodsWithDependenciesInOrder(def listAsOfNow, def listToIterateOver) {
        if (listToIterateOver.size() == 0) {
            return listAsOfNow
        }
        def element = listToIterateOver.find { candidate -> listAsOfNow.find { it.name == candidate.dependsOn } != null }
        if (!element) {
            throw new TestInitializationException("Your test methods are annotated with dependency that are invalid. Check @Test(dependsOnMethods = ... )", null)
        }
        return methodsWithDependenciesInOrder((listAsOfNow + element), (listToIterateOver - element))
    }

    private def methodsWithNoDependencies(def allMethods) {
        allMethods.findAll { it.dependsOn == null }
    }

    private boolean thereAreMethodsWithDependenciesIn(methodsList) {
        methodsList.find {it.dependsOn != null} != null
    }
}
