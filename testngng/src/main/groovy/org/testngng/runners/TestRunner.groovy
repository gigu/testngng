package org.testngng.runners

import org.testngng.domain.TestMethod
import org.testngng.domain.TestResult

import static org.testngng.utils.HappyBunchOfHelpers.timeTakenFrom

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 18/05/2012
 */
class TestRunner {

    def run(def testClassInstance, TestMethod testMethod) {
        if (testMethod.disabled) {
            return TestResult.skipped(testMethod.name)
        }
        def start = System.currentTimeMillis()
        def result
        try {
            if (testMethod.withDataProvider) {
                result = []
                def dataForTest
                try {
                    dataForTest = testClassInstance."$testMethod.dataProvider.methodName"()
                } catch (exception) {
                    return TestResult.failure(testMethod.name, exception.message, exception, timeTakenFrom(start))
                }
                dataForTest.each {
                    start = System.currentTimeMillis()
                    doTheStuffBeforeTest(testMethod, testClassInstance)
                    result << executeTest(testClassInstance, testMethod, "${testMethod.name} (${it.join(',')})", start, it)
                }

            } else {

                doTheStuffBeforeTest(testMethod, testClassInstance)
                result = executeTest(testClassInstance, testMethod, testMethod.name, start)
            }
            doTheStuffAfterTest(testMethod, testClassInstance)
        } catch (exception) {
            return TestResult.failure(testMethod.name, exception.message, exception, timeTakenFrom(start))
        }
        result
    }

    private def executeTest(def testClassInstance, def testMethod, def testName, def start, def dataProviderParameters = null) {
        try {
            if (dataProviderParameters) {
                def paramsAsList = []
                paramsAsList.addAll(dataProviderParameters)
                testClassInstance."$testMethod.name"(paramsAsList)
            } else {
                testClassInstance."$testMethod.name"()
            }
        } catch (Throwable exception) {
            if (expectedExcetpion(exception, testMethod)) {
                return TestResult.success(testName, timeTakenFrom(start))
            }
            return TestResult.failure(testName, exception.message, exception, timeTakenFrom(start))
        }
        TestResult.success(testName, timeTakenFrom(start))
    }

    private boolean expectedExcetpion(def throwable, TestMethod testMethod) {
        testMethod.expectedException.find { it == throwable.class } != null
    }

    private void doTheStuffAfterTest(TestMethod testMethod, testClassInstance) {
        testMethod.afterTest.each { testClassInstance."$it"() }
        testMethod.afterMethod.each { testClassInstance."$it"() }
    }

    private void doTheStuffBeforeTest(TestMethod testMethod, testClassInstance) {
        testMethod.beforeMethod.each { testClassInstance."$it"() }
        testMethod.beforeTest.each { testClassInstance."$it"() }
    }
}
