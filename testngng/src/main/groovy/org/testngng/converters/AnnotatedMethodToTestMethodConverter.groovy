package org.testngng.converters

import org.testng.annotations.Test
import org.testngng.domain.DataProvider
import org.testngng.domain.TestMethod
import org.testngng.exceptions.TestInitializationException
import org.testngng.finders.Methods

import static org.testngng.utils.HappyBunchOfHelpers.isNullOrEmpty
import org.testngng.domain.ClassAnnotationResults

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 18/05/2012
 */
class AnnotatedMethodToTestMethodConverter {

    def pleaseConvert(def toConvert, def setupMethods = [:]) {
        if (!toConvert || !toConvert[ClassAnnotationResults.METHOD] || !toConvert[ClassAnnotationResults.ANNOTATION]) {
            return TestMethod.EMPTY
        }


        new TestMethod(
                name: toConvert[ClassAnnotationResults.METHOD].name,
                enabled: toConvert[ClassAnnotationResults.ANNOTATION].enabled(),
                beforeMethod: setupMethods[Methods.BEFORE_METHOD],
                beforeTest: setupMethods[Methods.BEFORE_TEST],
                afterTest: setupMethods[Methods.AFTER_TEST],
                afterMethod: setupMethods[Methods.AFTER_METHOD],
                expectedException: toConvert[ClassAnnotationResults.ANNOTATION].expectedExceptions(),
                dependsOn: getMethodDependency(toConvert[ClassAnnotationResults.ANNOTATION]),
                dataProvider: dataProviderFrom(toConvert[ClassAnnotationResults.ANNOTATION], setupMethods[Methods.DATA_PROVIDER], toConvert[ClassAnnotationResults.METHOD].name)
        )
    }

    private DataProvider dataProviderFrom(Test annotation, def dataProviders, def methodName) {
        if (annotation.dataProviderClass() != Object) {
            throw new TestInitializationException("TestNGNG doesn't support DataProvider from a different classes [${methodName}]. It's stupid idea and makes tests hard to read.")
        }
        if (!isNullOrEmpty(annotation.dataProvider())) {
            if (!dataProviders || dataProviders.isEmpty() || !dataProviders.containsKey(annotation.dataProvider())) {
                throw new TestInitializationException("Your test [${methodName}] is pointing to a data provider [${annotation.dataProvider()}] that doesn't exists")
            }
            return dataProviders[annotation.dataProvider()]
        }
        null
    }

    private String getMethodDependency(Test annotation) {
        annotation.dependsOnMethods().length > 0 ? annotation.dependsOnMethods().first() : null
    }
}
