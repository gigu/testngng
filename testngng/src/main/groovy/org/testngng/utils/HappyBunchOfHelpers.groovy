package org.testngng.utils

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 07/06/2012
 */
class HappyBunchOfHelpers {

    static def formatedDuration(def timeInMilliseconds) {
        (timeInMilliseconds > 100) ? "${(timeInMilliseconds / 100)} s" : "${timeInMilliseconds} ms"
    }

    static long timeTakenFrom(long start) {
        return System.currentTimeMillis() - start
    }

    static boolean isNullOrEmpty(String string) {
        string == null || string.isEmpty()
    }

    static def copyFile(File source, File destination) {
        if (destination.exists()) {
            destination.delete()
        }
        def parentFolder = new File(destination.parent)
        if (!parentFolder.exists()) {
            parentFolder.mkdirs()
        }
        destination.createNewFile()
        destination.withOutputStream { out ->
            out.write(source.readBytes())
        }
    }

    static def copyStreamIntoFile(InputStream stream, File file) {
        def buffer = new byte[stream.available()]
        stream.read(buffer)
        file.withOutputStream { it.write(buffer) }
    }
}
