package org.testngng.utils

class Logger {
    private static def stdout
    private static def stderr

    private static def interceptingOut, interceptingErr

    private static def outStream, errorStream

    public static void initiateStreams() {
        stdout = System.out
        stderr = System.err
    }

    public static void startIntercepting() {
        outStream = new ByteArrayOutputStream()
        errorStream = new ByteArrayOutputStream()

        interceptingOut = new PrintStream(new OutputStream() {
            @Override
            void write(int b) {
                outStream.write(b)
            }
        }, true)

        interceptingErr = new PrintStream(new OutputStream() {
            @Override
            void write(int b) {
                errorStream.write(b)
            }
        }, true)

        System.out = interceptingOut
        System.err = interceptingErr
    }

    public static def stopIntercepting() {
        def intercepted = [out: outStream.toString(), err: errorStream.toString()]
        interceptingOut = interceptingErr = outStream = errorStream = null
        intercepted
    }

    public static void out(String text) {
        stdout.print(text)
        stdout.flush()
    }

    public static void outln(String text) {
        stdout.println(text)
        stdout.flush()
    }

    public static void error(String text) {
        stderr.print(text)
        stderr.flush()
    }

    public static void errorln(String text) {
        stderr.println(text)
        stderr.flush()
    }

}
