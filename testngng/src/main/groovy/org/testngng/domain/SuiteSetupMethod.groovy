package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 24/05/2012
 */
class SuiteSetupMethod {
    def name, declaringClass, instance

    @Override
    boolean equals(Object o) {
        if (o == null) return false
        if (!(o instanceof SuiteSetupMethod)) return false
        if (name != o.name) return false
        if (declaringClass != o.declaringClass) return false
        true
    }
}
