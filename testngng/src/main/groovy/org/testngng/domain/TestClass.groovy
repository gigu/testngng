package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 21/05/2012
 */
class TestClass {
    String name
    def testMethods = []
    def beforeClassMethods = [], afterClassMethods = []
    def instance

    static def createInstance(Class aClass) {
        aClass.newInstance()
    }
}
