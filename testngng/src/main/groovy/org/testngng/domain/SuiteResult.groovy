package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 23/05/2012
 */
class SuiteResult {
    def classResults = []
    def time = 0
    String suiteName
    def suiteResult
}
