package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 22/05/2012
 */
class TestSuite {
    static String DEFAULT_NAME = "default-suite"
    def name = DEFAULT_NAME
    def testClasses, beforeSuite, afterSuite
}
