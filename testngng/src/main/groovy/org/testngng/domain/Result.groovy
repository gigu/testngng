package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 24/05/2012
 */
enum Result {
    SUCCESS, FAILURE, SKIPPED
}
