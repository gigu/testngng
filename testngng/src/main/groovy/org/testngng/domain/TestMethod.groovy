package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 18/05/2012
 */
class TestMethod {
    static TestMethod EMPTY = new TestMethod()
    def name, enabled = true
    def beforeMethod, afterMethod
    def beforeTest, afterTest
    def expectedException
    def dependsOn
    def dataProvider

    def getDisabled() {
        !enabled
    }

    def getWithDataProvider() {
        dataProvider != null
    }
}
