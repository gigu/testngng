package org.testngng.domain

import org.testngng.utils.HappyBunchOfHelpers

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 18/05/2012
 */
class TestResult {
    static TestResult SUCCESS = new TestResult(result: Result.SUCCESS)
    static TestResult FAILURE = new TestResult(result: Result.FAILURE)
    static TestResult SKIPPED = new TestResult(result: Result.SKIPPED)

    def result, message, exception, testName, time = 0

    static TestResult failure(String testMethodName, String message, Throwable exception, Long time = 0) {
        new TestResult(result: Result.FAILURE, message: message, exception: exception, testName: testMethodName, time: time)
    }

    static TestResult success(String testMethodName, Long time = 0) {
        new TestResult(result: Result.SUCCESS, testName: testMethodName, time: time)
    }

    static TestResult skipped(String testMethodName) {
        new TestResult(result: Result.SKIPPED, testName: testMethodName)
    }

    static TestResult skipped(String testMethodName, Throwable exception) {
        new TestResult(result: Result.SKIPPED, testName: testMethodName, exception: exception)
    }

    def getDuration(){
        HappyBunchOfHelpers.formatedDuration(time)
    }

    def getStyle(){
        result == Result.SUCCESS ? 'success' : 'failures'
    }

    @Override
    boolean equals(Object o) {
        result == o.result
    }
}
