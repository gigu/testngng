package org.testngng.domain

import org.testngng.utils.HappyBunchOfHelpers

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 22/05/2012
 */
class ClassResult {
    def testResults = []
    def classResult
    def stdOutErr

    Long time = 0
    Long timestamp = 0
    String className

    def getName() {
        def split = className.split("\\.")
        split[split.size() - 1]
    }

    def getPackage() {
        def split = className.split("\\.")
        if (split.size() == 1) {
            return ""
        }
        split[0..split.length - 2].join(".")
    }

    def getFailures() {
        testResults.count { it == TestResult.FAILURE }
    }

    def getSuccessRate() {
       (failures > 0) ? Math.floor((testResults.size() - failures) / testResults.size() * 100) : 100
    }

    def getDuration(){
        HappyBunchOfHelpers.formatedDuration(time)
    }

    def getStyle(){
        (failures > 0) ? 'failures' : 'success'
    }

    def getOut(){
        stdOutErr?.out
    }

    def getErr(){
        stdOutErr?.err
    }
}
