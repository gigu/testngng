package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 16/05/2012
 */
class ClassAnnotationResults {
    static String METHOD = "METHOD"
    static String ANNOTATION = "ANNOTATION"
    def testMethods

    def getSize() {
        if (testMethods) {
            return testMethods.size()
        }
        0
    }

    def getAreThereAnyTestsInIt() {
        size > 0
    }
}
