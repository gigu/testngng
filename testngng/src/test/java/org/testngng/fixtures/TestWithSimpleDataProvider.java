package org.testngng.fixtures;

import org.testng.annotations.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 06/06/2012
 */
public class TestWithSimpleDataProvider {
    public int beforeTest = 0;
    public int beforeMethod = 0;

    @DataProvider(name = "provider1")
    public Object[][] makeMeSomeData() {
        return new Object[][]{
                {"1", 1},
                {"2", 2}
        };
    }

    @DataProvider
    public Object[][] makeMeSomeData2() {
        return new Object[][]{
                {"1", 1},
                {"2", 2}
        };
    }

    @Test(dataProvider = "provider1")
    public void testWithFirstDataProvider(String numberString, int number) {
        assertThat(number, is(Integer.parseInt(numberString)));
    }

    @Test(dataProvider = "makeMeSomeData2")
    public void testWithDefaultNamedDataProvider(String numberString, int number) {
        assertThat(number, is(Integer.parseInt(numberString)));
    }

    @BeforeTest
    public void doBeforeTest() {
        beforeTest++;
    }

    @BeforeMethod
    public void doBeforeMethod() {
        beforeMethod++;
    }

    @AfterClass
    public void afterClass() {
        System.out.println("Before test " + beforeTest);
        System.out.println("Before method " + beforeMethod);
    }
}
