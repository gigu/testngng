package org.testngng.fixtures;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 16/05/2012
 */
@Test
public class ClassWithTestAnnotatedOnClassItselfTest {
    public String variable = "";

    public void shouldTestFoo() {
        assertThat(true, is(true));
    }

    private void shouldntFail() {
        fail();
    }

    protected void shouldntFailToo() {
        fail();
    }

    @BeforeClass
    public void doBeforeClass() {
        variable = "doBeforeClass";
    }

    @AfterClass
    public void doAfterClass() {
        variable = "doAfterClass";
    }

    @DataProvider
    public Object[][] someDataProvider() {
        return new Object[1][1];
    }

}
