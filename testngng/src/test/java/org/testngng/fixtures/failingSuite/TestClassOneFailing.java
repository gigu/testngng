package org.testngng.fixtures.failingSuite;

import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.testng.FileAssert.fail;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
public class TestClassOneFailing {

    @Test
    public void shouldFailWithStringComparison() {
        assertThat("string", is("2"));
    }

    @Test
    public void shouldFailJustBecause() {
        fail("another string");
    }

    @Test
    public void shouldNotFail() {
        assertThat(0, is(0));
    }

    @Test
    public void shouldFailWithNumberComparison() {
        assertThat(0, is(1));
    }


}
