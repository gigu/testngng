package org.testngng.fixtures.failingSuite;

import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
public class TestClassTwoFailing {

    @Test
    public void testOne() {
        // Nothing
    }

    @Test
    public void testTwo() {
        assertThat("Buyaaa", is("Buyaaa"));
    }

    @Test(enabled = false)
    public void disabledTest() {
        fail("I should not be executed");
    }
}
