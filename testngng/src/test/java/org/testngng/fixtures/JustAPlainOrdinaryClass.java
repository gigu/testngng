package org.testngng.fixtures;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 21/05/2012
 */
public class JustAPlainOrdinaryClass {


    @SuppressWarnings("SomeWarning")
    public void doNothingWithAnotherAnnotation() {

    }

    @Deprecated
    public void doNothingWithAnnotation() {

    }


    public void doNothingMyFriend() {
        // Nothing
    }

}
