package org.testngng.fixtures;

import org.testng.annotations.*;

import static org.junit.Assert.assertTrue;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 24/05/2012
 */
public class TestClassExtendingFromBaseClass extends BaseForTestWithTestSetupMethods {

    @Test
    public void shouldPass() {
        assertTrue(true);
    }

    @Override
    @AfterSuite
    public void executeAfterSuite() {
        baseValue = "TestClassExtendingFromBaseClass.executeAfterSuite";
    }

    @BeforeSuite
    public void anotherBeforeSuite() {
        baseValue = "TestClassExtendingFromBaseClass.anotherBeforeSuite";
    }

    @BeforeClass
    public void doBeforeClassPlease() {

    }

    @AfterClass
    public void doStuffAfterClass() {

    }
}
