package org.testngng.fixtures;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 24/05/2012
 */
public abstract class BaseForTestWithTestSetupMethods {
    public String baseValue = "";

    @BeforeSuite
    public void executeBeforeSuite() {
        baseValue = "BaseForTestWithTestSetupMethods.executeBeforeSuite";
    }

    @AfterSuite
    public void executeAfterSuite() {
        baseValue = "BaseForTestWithTestSetupMethods.executeAfterSuite";
    }

    @BeforeClass
    public void executeBeforeClass() {
        baseValue = "BaseForTestWithTestSetupMethods.executeBeforeClass";
    }

    @AfterClass
    public void executeAfterClass() {
        baseValue = "BaseForTestWithTestSetupMethods.executeAfterClass";
    }

    @BeforeSuite
    protected void doOneMoreBeforeSuiteProtected() {
        // nothing
    }

    @BeforeSuite
    private void doOneMoreBeforeSuitePrivate() {
        // nothing
    }

}
