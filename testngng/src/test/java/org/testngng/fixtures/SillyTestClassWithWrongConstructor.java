package org.testngng.fixtures;

import org.testng.annotations.Test;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 22/05/2012
 */
@Test
public class SillyTestClassWithWrongConstructor {

    private String bolox;

    public SillyTestClassWithWrongConstructor(String bolox) {
        this.bolox = bolox;
    }
}
