package org.testngng.fixtures.successfulSuite;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
public class SomeHorribleStaticClassForSuccessfulSuite {
    public static String STATIC_FIELD_1 = "";
    public static String STATIC_FIELD_2 = "";
    public static String STATIC_FIELD_3 = "";
    public static String STATIC_FIELD_4 = "";
}
