package org.testngng.fixtures.successfulSuite;

import org.testng.annotations.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
public class TestClassTwo {

    private String field = "";
    private int number = 10;

    @BeforeSuite
    public void executeBeforeSuite() {
        SomeHorribleStaticClassForSuccessfulSuite.STATIC_FIELD_2 = "I was executed in Before Suite, WOHOOO!!!!";
    }

    @AfterClass
    public void resetAnotherStaticField() {
        SomeHorribleStaticClassForSuccessfulSuite.STATIC_FIELD_3 = "";
    }

    @BeforeMethod
    public void cleanNumber() {
        SomeHorribleStaticClassForSuccessfulSuite.STATIC_FIELD_3 = "Buyaaa";
    }

    @Test
    public void testOne() {
        assertThat(SomeHorribleStaticClassForSuccessfulSuite.STATIC_FIELD_3, is("Buyaaa"));
    }

    @Test(enabled = false)
    public void disabledTest() {
        fail("I should not be executed");
    }

    @AfterSuite
    public void cleanVariableAfterSuite() {
        SomeHorribleStaticClassForSuccessfulSuite.STATIC_FIELD_1 = "";
    }

}
