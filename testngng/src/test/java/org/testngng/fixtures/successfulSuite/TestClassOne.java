package org.testngng.fixtures.successfulSuite;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
public class TestClassOne {

    private String field = "";
    private int number = 10;
    private String dependentField = "";

    @BeforeSuite
    public void executeBeforeSuite() {
        SomeHorribleStaticClassForSuccessfulSuite.STATIC_FIELD_1 = "I was executed in Before Suite, WOHOOO!!!!";
    }

    @BeforeTest
    public void cleanField() {
        field = "";
    }

    @BeforeMethod
    public void cleanNumber() {
        number = 0;
    }

    @Test(dependsOnMethods = "makeSureNumberIsCleanedToZero")
    public void testEqualsOnTwoStrings() {
        assertThat(dependentField, is("string"));
    }

    @Test
    public void testEqualsOnStringsAndChangesFieldValue() {
        field = "Bolony";
        assertThat("another string", is("another string"));
    }

    @Test
    public void makeSureNumberIsCleanedToZero() {
        dependentField = "string";
        assertThat(number, is(0));
    }


}
