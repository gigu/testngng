package org.testngng.fixtures;

import org.testng.annotations.DataProvider;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 06/06/2012
 */
public class DataProviderClass {

    @DataProvider(name = "dataMaker")
    public static Object[][] makeMeSomeData() {
        return new Object[][]{
                {"10", 10},
                {"11", 11},
                {"12", 12}
        };
    }

    @DataProvider
    public static Object[][] makeMeSomeData2() {
        return new Object[][]{
                {"10", 10},
                {"11", 11},
                {"12", 12}
        };
    }

}
