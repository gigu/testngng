package org.testngng.fixtures;

import org.testng.annotations.Test;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
public class TestWithExpectedException {

    @Test(expectedExceptions = NumberFormatException.class)
    public void testWithExpectedException() {
        throw new NumberFormatException("Your number was in wrong format perhaps?");
    }

    @Test(expectedExceptions = NumberFormatException.class)
    public void expectingOneThingGotAnother() {
        throw new IllegalArgumentException("Looks wrong");
    }


}
