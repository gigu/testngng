package org.testngng.fixtures;

import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 15/05/2012
 */
public class ClassWithTestAnnotation {

    public static final String MESSAGE = "I'm failing you dear friends";
    public String someString = "";
    public boolean modified = false;

    @Test
    public void testThatDoesNothing() throws InterruptedException {
        sleepABit();
        assertTrue(true);
    }

    @Test
    public void thisPuppyShouldFail() throws InterruptedException {
        sleepABit();
        fail(MESSAGE);
    }

    @Test(enabled = false)
    public void shouldNotRun() {
        modified = true;
        assertTrue(true);
    }

    @Test
    public void shouldRunToo() {
        assertTrue(true);
    }

    @BeforeMethod
    public void doBeforeMethod() {
        someString = "doBeforeMethod";
    }

    @AfterMethod
    public void doSomethingAfterMethod() {
        someString = "doSomethingAfterMethod";
    }

    @BeforeTest
    public void doBeforeTest() {
        someString = "doBeforeTest";
    }

    @AfterTest
    public void doSomethingAfterTest() {
        someString = "doSomethingAfterTest";
    }

    @BeforeSuite
    public void doSomethingBeforeSuite() {
        someString = "doSomethingBeforeSuite";
    }

    @AfterSuite
    public void doSomethingAfterSuite() {
        someString = "doSomethingAfterSuite";
    }

    private void sleepABit() throws InterruptedException {
        Thread.sleep(50);
    }
}
