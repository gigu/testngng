package org.testngng.fixtures;

import org.testng.annotations.Test;

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
public class TestWithDependentTests {
    @Test
    public void test1() {

    }

    @Test(dependsOnMethods = "test3")
    public void test2() {

    }

    @Test(dependsOnMethods = "test1")
    public void test3() {

    }

    @Test
    public void test4() {

    }

    @Test(dependsOnMethods = "test4")
    public void test5() {

    }
}
