package org.testngng.assemblers

import org.testngng.domain.SuiteSetupMethod
import org.testngng.domain.TestClass
import org.testngng.domain.TestSuite
import org.testngng.finders.Methods
import org.testngng.finders.TestSetupMethodsFinder
import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.fixtures.TestClassExtendingFromBaseClass

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 29/05/2012
 */
class SuiteAssemblerTest extends GroovyTestCase {

    void test_assembledSuiteShouldContainOnlyUniqueSuiteSetupMethodsFromDeclaringClasses() {
        // Given
        def expectedName = "AcceptanceSuite"
        def methods = [:]
        def classInstance = new ClassWithTestAnnotation()
        methods[Methods.BEFORE_SUITE] = [new SuiteSetupMethod(name: "beforeSuite1", declaringClass: ClassWithTestAnnotation, instance: classInstance)]
        methods[Methods.AFTER_SUITE] = [new SuiteSetupMethod(name: "afterSuite", declaringClass: ClassWithTestAnnotation, instance: classInstance)]

        def testSetupMethodsFinder = [findMeSomeMethods: {def instance -> methods}] as TestSetupMethodsFinder
        def testClassAssembler = [assemble: {one, two, three -> return new TestClass(testMethods: ["method"])}] as TestClassAssembler
        def assembler = new SuiteAssembler(testSetupMethodsFinder: testSetupMethodsFinder, testClassAssembler: testClassAssembler)

        // When
        def suiteResult = assembler.assembleSuite([ClassWithTestAnnotation, TestClassExtendingFromBaseClass], expectedName)

        // Then
        assert suiteResult.beforeSuite.size() == 1
        assert suiteResult.afterSuite.size() == 1
        assert suiteResult.name == expectedName
    }

    void test_assembledSuiteShouldContainSuiteSetupMethods() {
        // Given
        def methods = [:]
        def classInstance = new ClassWithTestAnnotation()
        methods[Methods.BEFORE_SUITE] = [new SuiteSetupMethod(name: "beforeSuite1", declaringClass: ClassWithTestAnnotation, instance: classInstance)]
        methods[Methods.AFTER_SUITE] = [new SuiteSetupMethod(name: "afterSuite", declaringClass: ClassWithTestAnnotation, instance: classInstance)]

        def testSetupMethodsFinder = [findMeSomeMethods: {def instance -> methods}] as TestSetupMethodsFinder
        def testClassAssembler = [assemble: {one, two, three -> return new TestClass(testMethods: ["method"])}] as TestClassAssembler
        def assembler = new SuiteAssembler(testSetupMethodsFinder: testSetupMethodsFinder, testClassAssembler: testClassAssembler)

        // When
        def suiteResult = assembler.assembleSuite([ClassWithTestAnnotation])

        // Then
        assert suiteResult.beforeSuite.size() == 1
        assert suiteResult.beforeSuite[0] == methods[Methods.BEFORE_SUITE][0]
        assert suiteResult.afterSuite.size() == 1
        assert suiteResult.afterSuite[0] == methods[Methods.AFTER_SUITE][0]
    }

    void test_shouldAssembleSuite() {
        def testSetupMethodsFinder = [findMeSomeMethods: {def instance -> [:]}] as TestSetupMethodsFinder
        def testClassAssembler = [assemble: {one, two, three -> return new TestClass(testMethods: ["testMethod"])}] as TestClassAssembler
        def assembler = new SuiteAssembler(testSetupMethodsFinder: testSetupMethodsFinder, testClassAssembler: testClassAssembler)

        def suiteResult = assembler.assembleSuite([ClassWithTestAnnotation, TestClassExtendingFromBaseClass])

        assert suiteResult != null
        assert suiteResult instanceof TestSuite
        assert suiteResult.testClasses.size() == 2
    }
}
