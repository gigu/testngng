package org.testngng.assemblers

import org.testngng.converters.AnnotatedMethodToTestMethodConverter
import org.testngng.domain.TestSuite
import org.testngng.finders.TestAnnotationFinder
import org.testngng.finders.TestSetupMethodsFinder
import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.fixtures.JustAPlainOrdinaryClass
import org.testngng.fixtures.TestClassExtendingFromBaseClass

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 04/06/2012
 */
class SuiteAssemblerIntegrationTest extends GroovyTestCase {
    def suiteAssembler = new SuiteAssembler(
            testClassAssembler: new TestClassAssembler(
                    methodConverter: new AnnotatedMethodToTestMethodConverter(),
                    testAnnotationFinder: new TestAnnotationFinder()),
            testSetupMethodsFinder: new TestSetupMethodsFinder()
    )

    void test_shouldAssembleTheSuiteOfClasses() {
        def suite =
            suiteAssembler.assembleSuite([ClassWithTestAnnotation, JustAPlainOrdinaryClass, TestClassExtendingFromBaseClass])

        assert suite.name == TestSuite.DEFAULT_NAME
        assert suite.testClasses.size() == 2
        assert suite.beforeSuite.size() == 3
        assert suite.afterSuite.size() == 2
    }
}
