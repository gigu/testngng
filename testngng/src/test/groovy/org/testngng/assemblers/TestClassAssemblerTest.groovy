package org.testngng.assemblers

import org.testngng.converters.AnnotatedMethodToTestMethodConverter
import org.testngng.domain.ClassAnnotationResults
import org.testngng.domain.TestClass
import org.testngng.domain.TestMethod
import org.testngng.finders.Methods
import org.testngng.finders.TestAnnotationFinder
import org.testngng.fixtures.ClassWithTestAnnotation

import static org.testngng.utils.ReflectionHelper.gimmeMethodByNameFromClass

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 21/05/2012
 */
class TestClassAssemblerTest extends GroovyTestCase {

    void test_assembledClassShouldPassSetupMethodsIntoTestMethodConverter() {
        def expectedMethod = new TestMethod(name: "expectedName")
        def testMethod = gimmeMethodByNameFromClass("testThatDoesNothing", ClassWithTestAnnotation)

        def setupMethods = [:]
        setupMethods[Methods.BEFORE_TEST] = ["beforeTest"]
        setupMethods[Methods.BEFORE_METHOD] = ["beforeMethod"]
        setupMethods[Methods.AFTER_TEST] = ["afterTest"]
        setupMethods[Methods.AFTER_METHOD] = ["afterMethod"]

        def methodConverter = [pleaseConvert: { method, sm -> assert sm == setupMethods; return expectedMethod }] as AnnotatedMethodToTestMethodConverter

        def testAnnotationFinder = [findMeSomeTestsAndStuffInClass: { Class aClass ->
            return [testMethods: testMethod]
        }] as TestAnnotationFinder

        def assembler = new TestClassAssembler(methodConverter: methodConverter, testAnnotationFinder: testAnnotationFinder)
        assembler.assemble(ClassWithTestAnnotation, setupMethods, new ClassWithTestAnnotation())
    }

    void test_shouldUseMethodConverterToConvertMethods() {
        def expectedMethod = new TestMethod(name: "expectedName")
        def testMethod = gimmeMethodByNameFromClass("testThatDoesNothing", ClassWithTestAnnotation)


        def methodConverter = [pleaseConvert: { method, setupMethods -> return expectedMethod }] as AnnotatedMethodToTestMethodConverter
        def testAnnotationFinder = [findMeSomeTestsAndStuffInClass: { Class aClass ->
            return [testMethods: testMethod]
        }] as TestAnnotationFinder
        def classConverter = new TestClassAssembler(methodConverter: methodConverter, testAnnotationFinder: testAnnotationFinder)

        def convertedClass = classConverter.assemble(ClassWithTestAnnotation, [:], new ClassWithTestAnnotation())

        assert convertedClass.testMethods.size() == 1
        assert convertedClass.testMethods[0] == expectedMethod
    }

    void test_shouldConvertClassIntoTestClass() {
        def expectedAfterClassMethods = ["otherMethod"]
        def expectedBeforeClassMethods = ["doTheChacha"]

        def finder = [findMeSomeTestsAndStuffInClass: {Class aClass -> return new ClassAnnotationResults()}] as TestAnnotationFinder
        def classConverter = new TestClassAssembler(testAnnotationFinder: finder)

        def setupMethods = [:]
        setupMethods[Methods.BEFORE_CLASS] = expectedBeforeClassMethods
        setupMethods[Methods.AFTER_CLASS] = expectedAfterClassMethods

        def aClass = ClassWithTestAnnotation.class
        def expectedInstance = aClass.newInstance()
        TestClass result = classConverter.assemble(aClass, setupMethods, expectedInstance)

        assert result.name == ClassWithTestAnnotation.class.name

        assert result.instance == expectedInstance
        assert result.afterClassMethods == expectedAfterClassMethods
        assert result.beforeClassMethods == expectedBeforeClassMethods

    }
}
