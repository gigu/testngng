package org.testngng

import org.junit.After
import org.junit.Before
import org.testngng.utils.FileSystemHelper

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 14/06/2012
 */
class TestNGNGTest extends GroovyTestCase {

    File htmlFolder, xmlFolder

    @Before
    void setUp() {
        // Running from IDEA make sure the execution path is set to
        htmlFolder = new File("${FileSystemHelper.TEMP_TEST_FOLDER}/reports")
        xmlFolder = new File("${FileSystemHelper.TEMP_TEST_FOLDER}/test-results")

        def testngng = new TestNGNG(
                folderWithTestClasses: new File("./src/test/resources/compiled-classes"),
                xmlDestination: xmlFolder,
                htmlDestination: htmlFolder,
                suiteName: "Suite",
                includes: []
        )
        testngng.run()
    }

    void test_createFoldersWhenStartingTheTests() {
        assert htmlFolder.exists()

        assert xmlFolder.exists()
    }

    @After
    void tearDown(){
        FileSystemHelper.cleanFolderRecursively(htmlFolder)
        FileSystemHelper.cleanFolderRecursively(xmlFolder)
    }
}
