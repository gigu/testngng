package org.testngng.finders

import org.testng.annotations.Test
import org.testngng.domain.ClassAnnotationResults
import org.testngng.fixtures.ClassWithTestAnnotatedOnClassItselfTest
import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.fixtures.JustAPlainOrdinaryClass

import java.lang.reflect.Method

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 15/05/2012
 */
class TestAnnotationFinderTests extends GroovyTestCase {
    private def dearFinder = new TestAnnotationFinder()

    private def resultForClassWithAnnotatedMethods = dearFinder.findMeSomeTestsAndStuffInClass(ClassWithTestAnnotation)
    private def resultForClassWithAnnotatedClass = dearFinder.findMeSomeTestsAndStuffInClass(ClassWithTestAnnotatedOnClassItselfTest)
    private def classWithNotTestsWhatSoEver = dearFinder.findMeSomeTestsAndStuffInClass(JustAPlainOrdinaryClass)

    void test_resultsShouldContainBothMethodAndAnnotation(){
        resultForClassWithAnnotatedMethods.testMethods.each { methodEntry ->
            assert methodEntry[ClassAnnotationResults.METHOD] instanceof Method
            assert methodEntry[ClassAnnotationResults.ANNOTATION] instanceof Test
        }

        resultForClassWithAnnotatedClass.testMethods.each { methodEntry ->
            assert methodEntry[ClassAnnotationResults.METHOD] instanceof Method
            assert methodEntry[ClassAnnotationResults.ANNOTATION] instanceof Test
        }
    }

    void test_shouldSayThereAreTestsInAClass() {
        assert resultForClassWithAnnotatedClass.areThereAnyTestsInIt
        assert resultForClassWithAnnotatedMethods.areThereAnyTestsInIt
    }

    void test_shouldSayThereAreNoTestsInClass() {
        assert !classWithNotTestsWhatSoEver.areThereAnyTestsInIt
    }

    void test_shouldNotFindAnythingInTheClassWithNoTests() {
        assert classWithNotTestsWhatSoEver.size == 0
    }

    void test_shouldReturnTestMethodsFromAnnotatedClass() {
        assert resultForClassWithAnnotatedClass.size == 1
    }

    void test_shouldReturnClassAnnotationResult() {
        assert (resultForClassWithAnnotatedMethods instanceof ClassAnnotationResults)
    }

    void test_shouldFindTestMethodInClassAnnotatedWithTestNGTestAnotation() {
        assert resultForClassWithAnnotatedMethods.size == 4
    }
}
