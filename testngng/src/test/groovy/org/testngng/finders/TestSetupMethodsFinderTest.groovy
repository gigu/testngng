package org.testngng.finders

import org.testngng.fixtures.*

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 21/05/2012
 */
class TestSetupMethodsFinderTest extends GroovyTestCase {
    private def finder = new TestSetupMethodsFinder()
    private def result = finder.findMeSomeMethods(new ClassWithTestAnnotation())
    private def notMuchMethods = finder.findMeSomeMethods(new ClassWithTestAnnotatedOnClassItselfTest())
    private def resultsForExtendingClass = finder.findMeSomeMethods(new TestClassExtendingFromBaseClass())
    private def resultsForDataProvider = finder.findMeSomeMethods(new TestWithDataProvider())

    void test_shouldReturnEmptyListWhenNoMethodsFound() {
        assert notMuchMethods[Methods.BEFORE_METHOD] == []
        assert notMuchMethods[Methods.BEFORE_TEST] == []
        assert notMuchMethods[Methods.BEFORE_SUITE] == []
        assert notMuchMethods[Methods.AFTER_SUITE] == []
        assert notMuchMethods[Methods.AFTER_METHOD] == []
        assert notMuchMethods[Methods.AFTER_TEST] == []

        assert result[Methods.DATA_PROVIDER] == [:]
        assert result[Methods.BEFORE_CLASS] == []
        assert result[Methods.AFTER_CLASS] == []
    }

    void test_shouldFindDataProvider() {
        assert resultsForDataProvider[Methods.DATA_PROVIDER].size() == 2

        assert resultsForDataProvider[Methods.DATA_PROVIDER]["provider1"].name == "provider1"
        assert resultsForDataProvider[Methods.DATA_PROVIDER]["provider1"].methodName == "makeMeSomeData"

        assert resultsForDataProvider[Methods.DATA_PROVIDER]["makeMeSomeData2"].name == "makeMeSomeData2"
        assert resultsForDataProvider[Methods.DATA_PROVIDER]["makeMeSomeData2"].methodName == "makeMeSomeData2"
    }

    void test_shouldFindBeforeSuiteInBaseClass() {
        assert resultsForExtendingClass[Methods.BEFORE_SUITE].size() == 2
    }

    void test_shouldContainDataProvider() {
        assert notMuchMethods[Methods.DATA_PROVIDER].size() == 1
        assert notMuchMethods[Methods.DATA_PROVIDER]["someDataProvider"].methodName == "someDataProvider"
    }

    void test_shouldContainAfterClassFromBaseClass() {
        assert resultsForExtendingClass[Methods.AFTER_CLASS].size() == 2
    }

    void test_shouldContainBeforeClassFromBaseClass() {
        assert resultsForExtendingClass[Methods.BEFORE_CLASS].size() == 2
    }

    void test_shouldContainAfterClass() {
        def expectedMethod = "doAfterClass"
        assert notMuchMethods[Methods.AFTER_CLASS] == [expectedMethod]
    }

    void test_shouldContainBeforeClass() {
        def expectedMethod = "doBeforeClass"
        assert notMuchMethods[Methods.BEFORE_CLASS] == [expectedMethod]
    }

    void test_shouldContainOnlyOneAfterSuiteMethod() {
        def expectedMethod = "executeAfterSuite"

        assert resultsForExtendingClass[Methods.AFTER_SUITE].size() == 1
        assert resultsForExtendingClass[Methods.AFTER_SUITE][0].name == expectedMethod
        assert resultsForExtendingClass[Methods.AFTER_SUITE][0].declaringClass == TestClassExtendingFromBaseClass
        assert resultsForExtendingClass[Methods.AFTER_SUITE][0].instance instanceof TestClassExtendingFromBaseClass
    }

    void test_shouldContainBeforeFromBaseClass() {
        def expectedMethod = "executeBeforeSuite"

        assert resultsForExtendingClass[Methods.BEFORE_SUITE].size() == 2
        assert resultsForExtendingClass[Methods.BEFORE_SUITE][1].name == expectedMethod
        assert resultsForExtendingClass[Methods.BEFORE_SUITE][1].declaringClass == BaseForTestWithTestSetupMethods
        assert resultsForExtendingClass[Methods.BEFORE_SUITE][1].instance instanceof TestClassExtendingFromBaseClass
    }

    void test_shouldContainAfterSuite() {
        def expectedMethod = "doSomethingAfterSuite"

        assert result[Methods.AFTER_SUITE].size() == 1
        assert result[Methods.AFTER_SUITE][0].name == expectedMethod
        assert result[Methods.AFTER_SUITE][0].declaringClass == ClassWithTestAnnotation
    }

    void test_shouldContainBeforeSuite() {
        def expectedMethod = "doSomethingBeforeSuite"

        assert result[Methods.BEFORE_SUITE].size() == 1
        assert result[Methods.BEFORE_SUITE][0].name == expectedMethod
        assert result[Methods.BEFORE_SUITE][0].declaringClass == ClassWithTestAnnotation
    }

    void test_shouldContainAfterTest() {
        def expectedMethod = "doSomethingAfterTest"
        assert result[Methods.AFTER_TEST] == [expectedMethod]
    }

    void test_shouldContainAfterMethod() {
        def expectedMethod = "doSomethingAfterMethod"
        assert result[Methods.AFTER_METHOD] == [expectedMethod]
    }

    void test_shouldContainBeforeTest() {
        def expectedMethod = "doBeforeTest"
        assert result[Methods.BEFORE_TEST] == [expectedMethod]
    }

    void test_shouldContainBeforeMethod() {
        def expectedMethod = "doBeforeMethod"
        assert result[Methods.BEFORE_METHOD] == [expectedMethod]
    }

    void test_shouldFindBeforeTestMethodInTheClass() {
        assert result.size() == Methods.ALL_METHODS.size()

        Methods.ALL_METHODS.each { value ->
            assert result.containsKey(value)
        }
    }
}
