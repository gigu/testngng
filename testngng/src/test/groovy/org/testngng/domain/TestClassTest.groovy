package org.testngng.domain

import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.fixtures.SillyTestClassWithWrongConstructor

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 22/05/2012
 */
class TestClassTest extends GroovyTestCase {

    void test_shouldNotBlowWithExceptionWhenConstructorIsNotParameterlessAndUseDefault() {
        assert TestClass.createInstance(SillyTestClassWithWrongConstructor) instanceof SillyTestClassWithWrongConstructor
    }

    void test_shouldCreateNewInstanceOfAClassInstance() {
        assert TestClass.createInstance(ClassWithTestAnnotation) instanceof ClassWithTestAnnotation
    }
}
