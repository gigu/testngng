package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 13/06/2012
 */
class ClassResultTest extends GroovyTestCase {

    void test_shouldReturnFailuresStyleWhenFailures(){
        assert new ClassResult(testResults: [TestResult.success("a", 1), TestResult.failure("b", "", null, 1)]).style == 'failures'
    }

    void test_shouldFormatDuration(){
        assert new ClassResult(time: 12).duration == "12 ms"
        assert new ClassResult(time: 554).duration == "5.54 s"
    }

    void test_shouldReturnSuccessRate100PercentWhenNoFailures() {
        def testResults = [
                TestResult.success("1", 0),
                TestResult.success("2", 2),
        ]
        assert new ClassResult(testResults: testResults).successRate == 100
    }

    void test_shouldReturnSuccessRate() {
        def testResults = [
                TestResult.success("1", 0),
                TestResult.success("2", 2),
                TestResult.failure("3", "ad", null, 4)
        ]
        assert new ClassResult(testResults: testResults).successRate == Math.floor(2/3*100)
    }

    void test_shouldReturnNumberOfFailures() {
        def testResults = [
                TestResult.success("1", 0),
                TestResult.success("2", 2),
                TestResult.failure("3", "ad", null, 4)
        ]
        assert new ClassResult(testResults: testResults).failures == 1
        testResults = [
                TestResult.success("1", 0),
                TestResult.success("2", 2),
        ]
        assert new ClassResult(testResults: testResults).failures == 0
    }

    void test_shouldReturnPackageName() {
        assert new ClassResult(className: "Whateva").package == ""
        assert new ClassResult(className: "org.testngng.Whateva").package == "org.testngng"
    }

    void test_shouldReturnFullNameOfClassIfNoPackage() {
        assert new ClassResult(className: "Whateva").name == "Whateva"
    }

    void test_shouldReturnOnlyClassName() {
        assert new ClassResult(className: "com.foo.bar.Whateva").name == "Whateva"
    }
}
