package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 22/05/2012
 */
class TestResultTest extends GroovyTestCase {

    void test_shouldReturnStyleForSuccessAndFailures(){
        assert TestResult.success("a", 12).style == 'success'
        assert TestResult.failure("a","", null, 12).style == 'failures'
    }

    void test_shouldFormatDuration() {
        assert TestResult.success("foo", 12).duration == '12 ms'
    }

    void test_shouldCreateSkippedResult() {
        def testName = "awesomeTestName"

        def result = TestResult.skipped(testName)

        assert result == TestResult.SKIPPED
        assert result.testName == testName
    }

    void test_shouldCreateSuccessResult() {
        def testName = "awesomeTestName"

        def result = TestResult.success(testName)

        assert result == TestResult.SUCCESS
        assert result.testName == testName
    }

    void test_shouldCreateFailureWithExceptionMessageAndException() {
        def exception = new RuntimeException("Bolox")
        def message = "Wow, a massive failure"
        def methodName = "failingMethod"

        def result = TestResult.failure(methodName, message, exception)

        assert result.exception == exception
        assert result.message == message
        assert result.testName == methodName
        assert result == TestResult.FAILURE
    }
}
