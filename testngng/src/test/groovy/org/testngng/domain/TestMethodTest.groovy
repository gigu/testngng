package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 23/05/2012
 */
class TestMethodTest extends GroovyTestCase {

    void test_shouldNotIndicateThatTestShouldBeExecutedWithDataProvider() {
        assert !new TestMethod().withDataProvider
        assert !new TestMethod(dataProvider: null).withDataProvider
    }

    void test_shouldIndicateThatTestShouldBeExecutedWithDataProvider() {
        assert new TestMethod(dataProvider: "sadasd").withDataProvider
    }

    void test_shouldBeEnabledByDefault() {
        assert new TestMethod().enabled
    }

    void test_shouldReturnOpositeToEnabled() {
        assert new TestMethod(enabled: false).disabled
    }
}
