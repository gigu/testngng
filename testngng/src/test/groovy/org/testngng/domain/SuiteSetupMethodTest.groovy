package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 25/05/2012
 */
class SuiteSetupMethodTest extends GroovyTestCase {

    void test_shouldFindUniqueListOfSuiteMethods() {
        def m1 = new SuiteSetupMethod(name: "someName", declaringClass: String)
        def m2 = new SuiteSetupMethod(name: "someName", declaringClass: Long)
        def m3 = new SuiteSetupMethod(name: "another", declaringClass: String)
        def m4 = new SuiteSetupMethod(name: "someName", declaringClass: String)
        def m5 = new SuiteSetupMethod(name: "another", declaringClass: String)
        def m6 = new SuiteSetupMethod(name: "someName", declaringClass: Long)

        assert [m1, m2, m3, m4, m5, m6].unique() == [m1, m2, m3]
    }

    void test_shouldNotBeEqual() {
        def method = new SuiteSetupMethod(name: "foo", declaringClass: String)
        assert !method.equals(null)
        assert method != null
        assert method != ""
    }

    void test_shouldToEqualsByNameAndClass() {
        assert new SuiteSetupMethod(name: "foo", declaringClass: String) == new SuiteSetupMethod(name: "foo", declaringClass: String)
        assert new SuiteSetupMethod(name: "foo", declaringClass: String) != new SuiteSetupMethod(name: "f", declaringClass: String)
        assert new SuiteSetupMethod(name: "foo", declaringClass: String) != new SuiteSetupMethod(name: "foo", declaringClass: Long)
    }
}
