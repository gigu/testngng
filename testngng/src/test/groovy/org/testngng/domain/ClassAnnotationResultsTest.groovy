package org.testngng.domain

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 16/05/2012
 */
class ClassAnnotationResultsTest extends GroovyTestCase {

    void test_shouldReturnCorrectSize() {
        assert new ClassAnnotationResults().getSize() == 0
        assert new ClassAnnotationResults(testMethods: []).getSize() == 0
        assert new ClassAnnotationResults(testMethods: ["whateva", "another whateva"]).getSize() == 2
    }

}
