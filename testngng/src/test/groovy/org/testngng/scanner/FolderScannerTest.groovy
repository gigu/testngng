package org.testngng.scanner

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 13/06/2012
 */
class FolderScannerTest extends GroovyTestCase {

    void test_shouldFindClassesInWithIncludesTwo() {
        // To run this from IDE make sure yar working directory is right
        def folder = new File("./src/test/resources")

        def classes = new FolderScanner().scan(folder, ['integration'])

        assert classes.size() == 1
    }

    void test_shouldFindClassesInWithIncludes() {
        // To run this from IDE make sure yar working directory is right
        def folder = new File("./src/test/resources/compiled-classes")

        def classes = new FolderScanner().scan(folder, ['org'])

        assert classes.size() == 14
    }

    void test_shouldFindClassesInFolder()   {
        // To run this from IDE make sure yar working directory is right
        def folder = new File("./src/test/resources/compiled-classes")

        def classes = new FolderScanner().scan(folder)

        assert classes.size() == 14
    }

}
