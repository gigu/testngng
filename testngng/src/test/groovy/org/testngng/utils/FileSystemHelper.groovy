package org.testngng.utils

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 26/06/2012
 */
class FileSystemHelper {
    static String TEMP_TEST_FOLDER = "temp-test"

    static def cleanFolderRecursively(File folder) {
        if (!folder.isDirectory()) return
        folder.listFiles().each { file ->
            if (file.isDirectory()) {
                cleanFolderRecursively(file)
                file.deleteDir()
            }
            file.delete()
        }
        folder.deleteDir()
    }
}
