package org.testngng.utils

import java.lang.annotation.Annotation
import java.lang.reflect.Method

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 18/05/2012
 */
class ReflectionHelper {

    public static Method gimmeMethodByNameFromClass(String name, Class clazz) {
        clazz.declaredMethods.find { it.name == name }
    }

    public static Annotation gimmeAnnotationTypeFromMethod(Method method, Class typeOfAnnotation){
        method.annotations.find { it.annotationType() == typeOfAnnotation }
    }
}
