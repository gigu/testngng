package org.testngng.utils

import static org.testngng.utils.HappyBunchOfHelpers.isNullOrEmpty

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 07/06/2012
 */
class HappyBunchOfHelpersTest extends GroovyTestCase {

    File tmpFolder

    void test_shouldFormatDuration(){
        assert HappyBunchOfHelpers.formatedDuration(12) == '12 ms'
        assert HappyBunchOfHelpers.formatedDuration(120) == '1.2 s'
    }

    void test_streamCopyIsWorking(){
        def file = new File(tmpFolder, "inputStramFile.txt")
        HappyBunchOfHelpers.copyStreamIntoFile(new ByteArrayInputStream("foo".getBytes()), file)
        assert file.text == "foo"
    }

    void test_shouldCopyContentAndCreateSubDirectories() {
        def subFolder = new File(tmpFolder, "foo")

        def fileToCopy = new File(tmpFolder, "fileToCopy.txt")
        fileToCopy.text = "This is the awesome text in the file"

        def destination = new File(subFolder, "copiedFile.txt")
        HappyBunchOfHelpers.copyFile(fileToCopy, destination)

        assert destination.exists()
        assert destination.text == fileToCopy.text
    }

    void test_shouldCopyContentOfAFile() {
        def fileToCopy = new File(tmpFolder, "fileToCopy.txt")
        fileToCopy.text = "This is the awesome text in the file"

        def destination = new File(tmpFolder, "copiedFile.txt")
        HappyBunchOfHelpers.copyFile(fileToCopy, destination)

        assert destination.exists()
        assert destination.text == fileToCopy.text
    }

    void test_shouldSubtractFromCurrentTime() {
        assert HappyBunchOfHelpers.timeTakenFrom(System.currentTimeMillis() - 10) >= 10
    }

    void test_shouldNotBeEmptyStringWhenNotEmpty() {
        assert !isNullOrEmpty(" ")
        assert !isNullOrEmpty(" asdas ")
    }

    void test_shouldReportEmptyStringWhenEmpty() {
        assert isNullOrEmpty("")
    }

    void test_shouldReportEmptyStringWhenNull() {
        assert isNullOrEmpty(null)
    }

    void tearDown() {
        FileSystemHelper.cleanFolderRecursively(tmpFolder)
    }

    void setUp() {
        tmpFolder = new File("temp-test")
        tmpFolder.mkdir()
    }
}
