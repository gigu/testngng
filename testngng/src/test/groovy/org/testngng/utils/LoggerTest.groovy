package org.testngng.utils


class LoggerTest extends GroovyTestCase {

    void setUp(){
        Logger.initiateStreams()
    }

    void test_shouldInterceptPrintLn(){
        Logger.startIntercepting()

        print "Bar"
        System.err.print("Foo")

        def outputs = Logger.stopIntercepting()

        assert outputs.err == "Foo"
        assert outputs.out == "Bar"
    }

}
