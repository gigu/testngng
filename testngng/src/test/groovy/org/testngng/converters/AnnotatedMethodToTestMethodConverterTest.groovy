package org.testngng.converters

import org.testng.annotations.Test
import org.testngng.domain.ClassAnnotationResults
import org.testngng.domain.DataProvider
import org.testngng.domain.TestMethod
import org.testngng.exceptions.TestInitializationException
import org.testngng.finders.Methods
import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.fixtures.TestWithDataProvider
import org.testngng.fixtures.TestWithDependentTests
import org.testngng.fixtures.TestWithExpectedException
import org.testngng.utils.ReflectionHelper

import java.lang.reflect.Method

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 18/05/2012
 */
class AnnotatedMethodToTestMethodConverterTest extends GroovyTestCase {

    def mrConverter = new AnnotatedMethodToTestMethodConverter()

    void test_shouldBlowWithExceptionWhenDataProviderIsNotWithinSetupMethodsList() {
        def method = ReflectionHelper.gimmeMethodByNameFromClass("testWithFirstDataProvider", TestWithDataProvider)

        def setupMethods = [:]
        setupMethods[Methods.DATA_PROVIDER] = [:]
        try {
            mrConverter.pleaseConvert(toConvertFromMethod(method), setupMethods)
            fail("Should fail with exception")
        } catch (TestInitializationException ex) {
            assert ex.message == "Your test [testWithFirstDataProvider] is pointing to a data provider [provider1] that doesn't exists"
        }
    }

    void test_shouldNOTAddDataProviderClassToTestMethod() {
        def method = ReflectionHelper.gimmeMethodByNameFromClass("testWithDataProviderFromClass", TestWithDataProvider)

        try {
            mrConverter.pleaseConvert(toConvertFromMethod(method), [:])
            fail("This test should fail with exception")
        } catch (TestInitializationException e) {
            assert e.message == "TestNGNG doesn't support DataProvider from a different classes [testWithDataProviderFromClass]. It's stupid idea and makes tests hard to read."
        }
    }

    void test_shouldAddDataProviderDetailsToTestMethod() {
        def method = ReflectionHelper.gimmeMethodByNameFromClass("testWithFirstDataProvider", TestWithDataProvider)

        def setupMethods = [:]
        def expectedProvider = new DataProvider(name: "provider1", methodName: "makeMeSomeData")

        setupMethods[Methods.DATA_PROVIDER] = ["provider1": expectedProvider, "provider2": new DataProvider(name: "provider2", methodName: "anotherOne")]

        TestMethod converted = mrConverter.pleaseConvert(toConvertFromMethod(method), setupMethods)

        assert converted.dataProvider == expectedProvider
    }

    void test_shouldAddMethodsDependencyTestMethod() {
        def method = ReflectionHelper.gimmeMethodByNameFromClass("test2", TestWithDependentTests)

        TestMethod converted = mrConverter.pleaseConvert(toConvertFromMethod(method), [:])

        assert converted.dependsOn == "test3"
    }

    void test_shouldAddExceptionExpectationToConvertedMethod() {
        def method = ReflectionHelper.gimmeMethodByNameFromClass("testWithExpectedException", TestWithExpectedException)

        TestMethod converted = mrConverter.pleaseConvert(toConvertFromMethod(method), [:])

        assert converted.expectedException == [NumberFormatException]
    }

    void test_shouldAddTestSetupMethodsToConvertedTest() {
        def setupMethods = [:]
        setupMethods[Methods.BEFORE_METHOD] = ["beforeMethod"]
        setupMethods[Methods.BEFORE_TEST] = ["beforeTest"]
        setupMethods[Methods.AFTER_METHOD] = ["afterMethod"]
        setupMethods[Methods.AFTER_TEST] = ["afterTest"]

        def method = ReflectionHelper.gimmeMethodByNameFromClass("testThatDoesNothing", ClassWithTestAnnotation.class)

        def result = mrConverter.pleaseConvert(toConvertFromMethod(method), setupMethods)

        assert result.beforeMethod == setupMethods[Methods.BEFORE_METHOD]
        assert result.beforeTest == setupMethods[Methods.BEFORE_TEST]
        assert result.afterMethod == setupMethods[Methods.AFTER_METHOD]
        assert result.afterTest == setupMethods[Methods.AFTER_TEST]
    }

    void test_shouldAddInformationForDisabledTests() {
        def methodName = "testThatDoesNothing"
        def method = ReflectionHelper.gimmeMethodByNameFromClass(methodName, ClassWithTestAnnotation.class)

        def result = mrConverter.pleaseConvert(toConvertFromMethod(method))
        assert result.enabled

        methodName = "shouldNotRun"
        method = ReflectionHelper.gimmeMethodByNameFromClass(methodName, ClassWithTestAnnotation.class)

        result = mrConverter.pleaseConvert(toConvertFromMethod(method))
        assert !result.enabled
    }

    void test_shouldConvertMethodName() {
        def funkyMethodName = "testThatDoesNothing"
        def method = ReflectionHelper.gimmeMethodByNameFromClass(funkyMethodName, ClassWithTestAnnotation.class)

        def toConvert = toConvertFromMethod(method)
        def result = mrConverter.pleaseConvert(toConvert)
        assert result.name == funkyMethodName
    }

    void test_shouldReturnEmptyResultIfMethodIsNull() {
        def result = mrConverter.pleaseConvert(null)
        assert result == TestMethod.EMPTY
    }

    private def toConvertFromMethod(Method method) {
        def toConvert = [:]
        toConvert[ClassAnnotationResults.METHOD] = method
        toConvert[ClassAnnotationResults.ANNOTATION] = ReflectionHelper.gimmeAnnotationTypeFromMethod(method, Test)
        toConvert
    }
}
