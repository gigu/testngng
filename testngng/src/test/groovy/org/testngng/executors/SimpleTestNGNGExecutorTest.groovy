package org.testngng.executors

import org.testngng.assemblers.SuiteAssembler
import org.testngng.domain.SuiteResult
import org.testngng.domain.TestSuite
import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.fixtures.TestClassExtendingFromBaseClass
import org.testngng.runners.SuiteRunner

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 04/06/2012
 */
class SimpleTestNGNGExecutorTest extends GroovyTestCase {

    void test_shouldAssembleTestSuiteAndExecuteTests() {
        def expectedResult = new SuiteResult()

        def assembler = [assembleSuite: {def listOfClasses, suiteName -> new TestSuite()}] as SuiteAssembler
        def runner = [run: {def testSuite -> expectedResult}] as SuiteRunner

        def executor = new SimpleTestNGNGExecutor(suiteAssembler: assembler, suiteRunner: runner)

        def suiteResult = executor.execute([ClassWithTestAnnotation, TestClassExtendingFromBaseClass])

        assert suiteResult == expectedResult
    }
}

