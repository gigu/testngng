package org.testngng.executors

import org.testngng.domain.ClassResult
import org.testngng.domain.Result
import org.testngng.domain.SuiteResult
import org.testngng.domain.TestResult
import org.testngng.exceptions.TestInitializationException
import org.testngng.utils.FileSystemHelper

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 25/06/2012
 */
class HtmlReportingTestNGNGExecutorTest extends GroovyTestCase {
    def simpleTestNGNGExecutor = new TestNGNGExecutorsFactory().simpleTestNGNGExecutor()
    File htmlFolder, htmlFolderForFailing

    void test_shouldCreateHtmlReportsForFailingSuite() {
        createFailingReport()

        def index = new File(htmlFolderForFailing, "suite-SuiteName-Fail.html")
        assert index.exists()
    }

    void test_shouldCreateHtmlReportsForClasses() {
        createSampleReport()

        def barReport = new File(htmlFolder, "com.foo.Bar.html")
        def barBarReport = new File(htmlFolder, "com.foo.BarBar.html")

        assert barReport.exists()
        assert barBarReport.exists()
    }

    void test_shouldCreateHtmlReportsForPackage() {
        createSampleReport()

        def packageReport = new File(htmlFolder, "com.foo.html")

        assert packageReport.exists()
        assert packageReport.text.contains('com.foo')
        assert packageReport.text.contains('Bar')
        assert packageReport.text.contains('BarBar')
        assert packageReport.text.contains('href="com.foo.Bar.html"')
        assert packageReport.text.contains('href="com.foo.BarBar.html"')
    }

    void test_shouldCreateHtmlReportsSummaryForPackagesAndClasses() {
        createSampleReport()

        def suiteHtmlSummaryPage = new File(htmlFolder, "suite-SuiteName-Special.html")

        assert suiteHtmlSummaryPage.exists()

        assert suiteHtmlSummaryPage.text.contains("com.foo")
        assert suiteHtmlSummaryPage.text.contains("com.foo.Bar")
        assert suiteHtmlSummaryPage.text.contains('href="com.foo.html"')
        assert suiteHtmlSummaryPage.text.contains('href="com.foo.Bar.html"')
        assert suiteHtmlSummaryPage.text.contains('href="com.foo.BarBar.html"')
    }

    void test_shouldCopyStaticFiles() {
        def expectedStaticFiles = HtmlReportingTestNGNGExecutor.STATIC_FILES
        def expectedResult = new SuiteResult(suiteResult: Result.SUCCESS, suiteName: "foo-bar")
        def executor = [execute: { classList, suiteName -> expectedResult }] as SimpleTestNGNGExecutor

        def htmlExecutor = new HtmlReportingTestNGNGExecutor(testNGNGExecutor: executor, htmlDestinationFolder: htmlFolder)

        htmlExecutor.execute([])

        expectedStaticFiles.each { fileName ->
            assert new File(htmlFolder, fileName).exists()
        }
    }

    void test_shouldUseSimpleExecutorToRunTests() {
        def expectedName = 'whateva'
        def expectedResult = new SuiteResult(suiteResult: Result.SUCCESS, suiteName: "foo-bar")
        def executor = [execute: { classList, suiteName -> expectedResult }] as SimpleTestNGNGExecutor

        def suiteResult = new HtmlReportingTestNGNGExecutor(testNGNGExecutor: executor, htmlDestinationFolder: htmlFolder).execute([])

        assert suiteResult == expectedResult
    }

    void test_shouldBlowIfFolderIsEmpty() {
        try {
            new HtmlReportingTestNGNGExecutor(testNGNGExecutor: simpleTestNGNGExecutor).execute([])
            fail("Should fail with test init exception")
        } catch (TestInitializationException e) {
            assert e.message == "The reports folder for HTML is not set"
        }
    }

    private void createFailingReport() {
        def testResults = [
                TestResult.success("testMethod1", 1),
                TestResult.success("testMethod2", 2),
                TestResult.failure("testMethod3", "Failed", null, 12),
                TestResult.success("testMethod4", 5),
        ]
        def testResultsWithoutFailure = [
                TestResult.success("testMethod1", 1),
                TestResult.success("testMethod2", 2),
                TestResult.success("testMethod4", 5),
        ]
        def classResult = [
                new ClassResult(className: "taram.tatam.SomeClassTest",
                        testResults: testResults,
                        timestamp: System.currentTimeMillis(),
                        time: 10),
                new ClassResult(className: "taram.tatam.SomeOtherClassTest",
                        testResults: testResults,
                        timestamp: System.currentTimeMillis(),
                        time: 40),
                new ClassResult(className: "taram.tatam.AndAnotherClassTest",
                        testResults: testResultsWithoutFailure,
                        timestamp: System.currentTimeMillis(),
                        time: 40),
                new ClassResult(className: "taram.tatam.boom.SomeOtherOneTest",
                        testResults: testResultsWithoutFailure,
                        timestamp: System.currentTimeMillis(),
                        time: 40),
        ]
        def suiteResult = new SuiteResult(suiteName: "SuiteName-Fail", classResults: classResult, time: 10, suiteResult: Result.FAILURE)
        def executor = [execute: { classList, suiteName -> suiteResult }] as SimpleTestNGNGExecutor

        def htmlExecutor = new HtmlReportingTestNGNGExecutor(testNGNGExecutor: executor, htmlDestinationFolder: htmlFolderForFailing)

        htmlExecutor.execute([])

    }

    private void createSampleReport() {
        def testResults = [
                TestResult.success("testMethod1", 1),
                TestResult.success("testMethod2", 2)
        ]
        def classResult = [
                new ClassResult(className: "com.foo.Bar",
                        testResults: testResults,
                        timestamp: System.currentTimeMillis(),
                        time: 10),
                new ClassResult(className: "com.foo.BarBar",
                        testResults: testResults,
                        timestamp: System.currentTimeMillis(),
                        time: 40),
        ]
        def suiteResult = new SuiteResult(suiteName: "SuiteName-Special", classResults: classResult, time: 10, suiteResult: Result.SUCCESS)
        def executor = [execute: { classList, suiteName -> suiteResult }] as SimpleTestNGNGExecutor

        def htmlExecutor = new HtmlReportingTestNGNGExecutor(testNGNGExecutor: executor, htmlDestinationFolder: htmlFolder)

        htmlExecutor.execute([])
    }

    void setUp() {
        htmlFolder = new File("temp-test/html")
        htmlFolderForFailing = new File("temp-test/htmlFailed")
        FileSystemHelper.cleanFolderRecursively(htmlFolder)
        FileSystemHelper.cleanFolderRecursively(htmlFolderForFailing)
        htmlFolder.mkdirs()
        htmlFolderForFailing.mkdirs()

    }

    void tearDown() {
        FileSystemHelper.cleanFolderRecursively(htmlFolder)
        FileSystemHelper.cleanFolderRecursively(htmlFolderForFailing)
    }

}
