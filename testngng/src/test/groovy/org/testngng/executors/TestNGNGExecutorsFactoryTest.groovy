package org.testngng.executors

import org.testngng.reporters.ClassResultXmlReporter

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
class TestNGNGExecutorsFactoryTest extends GroovyTestCase {

    void test_shouldReturnHtmlReportingTestNGNGExecutorWithXmlReportingTestNGNGExecutorWhenHtmlFolderAndXmlFolderIsSet() {
        def executor = new TestNGNGExecutorsFactory().executor(new File("."), new File("."))

        assert executor instanceof HtmlReportingTestNGNGExecutor
        assert executor.testNGNGExecutor instanceof XmlReportingTestNGNGExecutor
        assert ((XmlReportingTestNGNGExecutor) executor.testNGNGExecutor).
                simpleTestNGNGExecutor.suiteRunner.classRunner.classResultXmlReporter instanceof ClassResultXmlReporter

    }

    void test_shouldReturnHtmlReportingTestNGNGExecutorWithSimpleTestNGNGExecutorWhenHtmlFolderIsSet() {
        def executor = new TestNGNGExecutorsFactory().executor(null, new File("."))

        assert executor instanceof HtmlReportingTestNGNGExecutor
        assert executor.testNGNGExecutor instanceof SimpleTestNGNGExecutor
    }

    void test_shouldReturnXmlReportingTestNGNGExecutorWithSimpleTestNGNGExecutorWhenXmlFolderIsSet() {
        def executor = new TestNGNGExecutorsFactory().executor(new File("."), null)

        assert executor instanceof XmlReportingTestNGNGExecutor
        assert executor.simpleTestNGNGExecutor instanceof SimpleTestNGNGExecutor
    }

    void test_shouldReturnSimpleTestNGNGExecutorWhenHtmlFolderAndXmlFolderIsNotSet() {
        def executor = new TestNGNGExecutorsFactory().executor(null, null)

        assert executor instanceof SimpleTestNGNGExecutor
    }
}
