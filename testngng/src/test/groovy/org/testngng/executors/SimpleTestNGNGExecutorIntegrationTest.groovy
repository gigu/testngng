package org.testngng.executors

import org.testngng.domain.Result
import org.testngng.domain.SuiteResult
import org.testngng.fixtures.failingSuite.TestClassOneFailing
import org.testngng.fixtures.failingSuite.TestClassTwoFailing
import org.testngng.fixtures.successfulSuite.TestClassOne
import org.testngng.fixtures.successfulSuite.TestClassTwo

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
class SimpleTestNGNGExecutorIntegrationTest extends GroovyTestCase {
    def executor = new TestNGNGExecutorsFactory().simpleTestNGNGExecutor()

    void test_shouldRunSuccessfulSuiteAndReportSuccessfulResults() {
        SuiteResult results = executor.execute([TestClassOne, TestClassTwo])

        assert results.suiteResult == Result.SUCCESS
    }

    void test_shouldRunFailingSuiteAndReportFailure() {
        SuiteResult result = executor.execute([TestClassOneFailing, TestClassTwoFailing])

        assert result.suiteResult == Result.FAILURE
    }
}
