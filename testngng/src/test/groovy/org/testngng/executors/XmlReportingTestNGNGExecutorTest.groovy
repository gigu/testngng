package org.testngng.executors

import org.testngng.exceptions.TestInitializationException
import org.testngng.utils.FileSystemHelper

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 12/06/2012
 */
class XmlReportingTestNGNGExecutorTest extends GroovyTestCase {

    void test_shouldThrowExceptionIfReportsFolderIsNotSet() {
        try {
            new XmlReportingTestNGNGExecutor().execute([])
            fail('Should fail')
        } catch (TestInitializationException ex) {
            assert ex.message == 'XML test reports folder is not set'
        }
    }

    void test_shouldCheckIfTheFolderIsSetAndCreateIfItDoesntExists() {
        def folder = new File("$FileSystemHelper.TEMP_TEST_FOLDER/foo")
        FileSystemHelper.cleanFolderRecursively(folder)

        def executor = [execute: {classList, suiteName -> [classResults: []]}] as SimpleTestNGNGExecutor
        new XmlReportingTestNGNGExecutor(reportsFolder: folder, simpleTestNGNGExecutor: executor).execute([])

        assert folder.exists()
        FileSystemHelper.cleanFolderRecursively(folder)
    }

}
