package org.testngng.runners

import org.testngng.domain.*

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 23/05/2012
 */
class SuiteRunnerTests extends GroovyTestCase {

    void test_shouldReportSuiteAsSkippedIfBeforeSuiteFails(){
        def classRunner = [:] as ClassRunner
        def suiteRunner = new SuiteRunner(classRunner: classRunner)
        def testClasses = [new TestClass()]

        def testSuite = new TestSuite(testClasses: testClasses, beforeSuite: new SuiteSetupMethod(instance: {return ""}, name: 'beforeSuite'))

        def result = suiteRunner.run(testSuite)

        assert result.suiteResult == Result.SKIPPED
        assert result.time == 0
        assert result.classResults == []
    }

    void test_shouldContainSuiteExecutionInformation() {
        def classRunner = [run: {testClass -> Thread.sleep(50); new ClassResult()}] as ClassRunner

        def suiteRunner = new SuiteRunner(classRunner: classRunner)
        def testClasses = [new TestClass()]

        def result = suiteRunner.run(new TestSuite(testClasses: testClasses))

        assert result.time >= 1
    }

    void test_shouldRunAfterSuiteWhenClassesAreExecuted() {
        def classRunner = [run: {testClass -> new ClassResult()}] as ClassRunner
        def suiteRunner = new SuiteRunner(classRunner: classRunner)
        def testClasses = [new TestClass()]
        def itWasExecuted = false
        def afterSuite = [new SuiteSetupMethod(name: "methodName", instance: [methodName: {-> itWasExecuted = true}], declaringClass: "SomeClass")]

        suiteRunner.run(new TestSuite(testClasses: testClasses, afterSuite: afterSuite))

        assert itWasExecuted
    }

    void test_shouldRunBeforeSuiteBeforeExecutingClasses() {
        def classRunner = [run: {testClass -> new ClassResult()}] as ClassRunner
        def suiteRunner = new SuiteRunner(classRunner: classRunner)
        def testClasses = [new TestClass()]
        def itWasExecuted = false
        def beforeSuite = [new SuiteSetupMethod(name: "methodName", instance: [methodName: {-> itWasExecuted = true}], declaringClass: "SomeClass")]

        suiteRunner.run(new TestSuite(testClasses: testClasses, beforeSuite: beforeSuite))

        assert itWasExecuted
    }

    void test_shouldRunClassesFromSuite() {
        def classRunner = [run: {testClass -> new ClassResult(classResult: Result.SUCCESS)}] as ClassRunner
        def suiteRunner = new SuiteRunner(classRunner: classRunner)
        def testClasses = [new TestClass(), new TestClass()]

        def results = suiteRunner.run(new TestSuite(testClasses: testClasses))

        assert results != null
        assert results instanceof SuiteResult
        assert results.suiteResult == Result.SUCCESS
        assert results.classResults.size() == 2
    }

    void test_shouldReportSuiteAsFailedIfOneClassReportsAsFailed() {
        def classRunner = [run: {testClass -> new ClassResult(classResult: Result.FAILURE)}] as ClassRunner
        def suiteRunner = new SuiteRunner(classRunner: classRunner)
        def testClasses = [new TestClass(), new TestClass()]

        def results = suiteRunner.run(new TestSuite(testClasses: testClasses))

        assert results.suiteResult == Result.FAILURE
    }

    void test_shouldAddSuiteNameToSuiteResult() {
        def expectedName = "SuperDuperAwesomeName"
        def classRunner = [run: {testClass -> new ClassResult()}] as ClassRunner
        def suiteRunner = new SuiteRunner(classRunner: classRunner)
        def testClasses = [new TestClass(), new TestClass()]

        def results = suiteRunner.run(new TestSuite(testClasses: testClasses, name: expectedName))

        assert results.suiteName == expectedName
    }
}
