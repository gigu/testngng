package org.testngng.runners

import org.testngng.domain.DataProvider
import org.testngng.domain.Result
import org.testngng.domain.TestMethod
import org.testngng.domain.TestResult
import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.fixtures.TestWithExpectedException
import org.testngng.fixtures.TestWithSimpleDataProvider

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 18/05/2012
 */
class TestRunnerTests extends GroovyTestCase {
    private def runForest = new TestRunner()

    def resultOfMethodThatDoesNothing = runForest.run(new ClassWithTestAnnotation(), new TestMethod(name: "testThatDoesNothing"))
    def resultOfMethodThatFails = runForest.run(new ClassWithTestAnnotation(), new TestMethod(name: "thisPuppyShouldFail"))

    void test_beforeTestSetupMethodShouldBeExecutedForEachIterationOfDataProvider() {
        def instance = new TestWithSimpleDataProvider()
        def testMethod = new TestMethod(
                name: "testWithFirstDataProvider",
                dataProvider: new DataProvider(name: "provider1", methodName: 'makeMeSomeData'),
                beforeTest: ['doBeforeTest'],
                beforeMethod: ['doBeforeMethod']
        )

        runForest.run(instance, testMethod)

        assert instance.beforeMethod == 2
        assert instance.beforeTest == 2
    }

    void test_shouldFailTestWhenBeforeSetupIsFailing(){
        def instance = new TestWithSimpleDataProvider()
        def testMethod = new TestMethod(name: 'testWithFirstDataProvider', beforeMethod: ['fooBar'])

        def result = runForest.run(instance, testMethod)
        assert result.result == Result.FAILURE

        testMethod = new TestMethod(name: 'testWithFirstDataProvider', beforeTest: ['fooBar'])

        result = runForest.run(instance, testMethod)
        assert result.result == Result.FAILURE
    }

    void test_shouldSkipTestsWithFailingDataProvider() {
        def instance = new TestWithSimpleDataProvider()
        def testMethod = new TestMethod(name: "testWithFirstDataProvider", dataProvider: new DataProvider(name: "provider1", methodName: "notRealMethod"))

        def result = runForest.run(instance, testMethod)

        assert result.result == Result.FAILURE
        assert result.testName == "testWithFirstDataProvider"
    }

    void test_failingTestsShouldContainTimingInformation() {
        assert resultOfMethodThatFails.time > 0
    }

    void test_resultShouldContainTiming() {
        assert resultOfMethodThatDoesNothing.time > 0
    }

    void test_shouldRunSameTestMultipleTimesWithDataProvidedFromDataProvider() {
        def instance = new TestWithSimpleDataProvider()
        def testMethod = new TestMethod(name: "testWithFirstDataProvider", dataProvider: new DataProvider(name: "provider1", methodName: "makeMeSomeData"))

        def result = runForest.run(instance, testMethod)

        assert result.size() == 2
        assert result[0].testName == "testWithFirstDataProvider (1,1)"
        assert result[1].testName == "testWithFirstDataProvider (2,2)"
    }

    void test_shouldFailWithFailureReportingOriginalExceptionWhenExpectedExceptionListIsEmpty() {
        def testClass = new TestWithExpectedException()

        TestResult result = runForest.run(testClass, new TestMethod(name: "expectingOneThingGotAnother"))

        assert result.result == Result.FAILURE
    }

    void test_shouldFailIfTestFailsWithDifferentThanExpectedException() {
        def testClass = new TestWithExpectedException()

        TestResult result = runForest.run(testClass, new TestMethod(name: "expectingOneThingGotAnother", expectedException: [NumberFormatException]))

        assert result.result == Result.FAILURE
    }

    void test_shouldNotFailIfTestFailsWithExpectedException() {
        def testClass = new TestWithExpectedException()

        TestResult result = runForest.run(testClass, new TestMethod(name: "testWithExpectedException", expectedException: [NumberFormatException]))

        assert result == TestResult.SUCCESS
    }

    void test_shouldNotBotherWithRunningWhenTestIsDisabled() {
        def testClass = new ClassWithTestAnnotation()

        def result = runForest.run(testClass, new TestMethod(name: "shouldNotRun", enabled: false))

        assert !testClass.modified
        assert result == TestResult.SKIPPED
        assert result.time == 0
    }

    void test_shouldInvokeMethodAnnotatedWithAfterTest() {
        def testClass = new ClassWithTestAnnotation()

        assert testClass.someString == ""

        runForest.run(testClass, new TestMethod(name: "testThatDoesNothing", afterTest: ["doSomethingAfterTest"]))

        assert testClass.someString == "doSomethingAfterTest"
    }

    void test_shouldInvokeMethodAnnotatedWithBeforeTest() {
        def testClass = new ClassWithTestAnnotation()

        assert testClass.someString == ""

        runForest.run(testClass, new TestMethod(name: "testThatDoesNothing", beforeTest: ["doBeforeTest"]))

        assert testClass.someString == "doBeforeTest"
    }

    void test_shouldInvokeMethodAnnotatedWithAfterMethod() {
        def testClass = new ClassWithTestAnnotation()

        assert testClass.someString == ""

        runForest.run(testClass, new TestMethod(name: "testThatDoesNothing", afterMethod: ["doSomethingAfterMethod"]))

        assert testClass.someString == "doSomethingAfterMethod"
    }

    void test_shouldInvokeMethodAnnotatedWithBeforeMethod() {
        def testClass = new ClassWithTestAnnotation()

        assert testClass.someString == ""

        runForest.run(testClass, new TestMethod(name: "testThatDoesNothing", beforeMethod: ["doBeforeMethod"]))

        assert testClass.someString == "doBeforeMethod"
    }

    void test_shouldFailWhenMethodFail() {
        assert resultOfMethodThatFails == TestResult.FAILURE
        assert resultOfMethodThatFails.message == ClassWithTestAnnotation.MESSAGE
    }

    void test_shouldExecuteSingleTestAndBeSuccesfull() {
        assert resultOfMethodThatDoesNothing == TestResult.SUCCESS
    }
}
