package org.testngng.runners

import org.testngng.domain.TestMethod
import org.testngng.exceptions.TestInitializationException

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 05/06/2012
 */
class TestMethodsSorterTest extends GroovyTestCase {
    TestMethodsSorter sorter = new TestMethodsSorter()

    void test_shouldSortMoreComplexDependency() {
        def methodOne = new TestMethod(name: "one", dependsOn: "four")
        def methodTwo = new TestMethod(name: "two")
        def methodThree = new TestMethod(name: "three", dependsOn: "five")
        def methodFour = new TestMethod(name: "four")
        def methodFive = new TestMethod(name: "five", dependsOn: "one")
        def methodSix = new TestMethod(name: "six", dependsOn: "one")


        def methods = [methodOne, methodTwo, methodThree, methodFour, methodFive, methodSix]

        def sorted = sorter.sort(methods)

        assert sorted == [methodTwo, methodFour, methodOne, methodFive, methodThree, methodSix]
    }

    void test_shouldThrowExceptionIfTestDependencyDoesntExists() {
        def methodOne = new TestMethod(name: "one", dependsOn: "two")
        def methodTwo = new TestMethod(name: "two", dependsOn: "three")

        def methods = [methodOne, methodTwo]
        try {
            sorter.sort(methods)
            fail("Should fail with an exception")
        } catch (TestInitializationException ex) {
            assert ex.message == "Your test methods are annotated with dependency that are invalid. Check @Test(dependsOnMethods = ... )"
        }
    }

    void test_shouldThrowExceptionIfTestDependencyInvalid() {
        def methodOne = new TestMethod(name: "one", dependsOn: "two")
        def methodTwo = new TestMethod(name: "two", dependsOn: "one")

        def methods = [methodOne, methodTwo]
        try {
            sorter.sort(methods)
            fail("Should fail with an exception")
        } catch (TestInitializationException ex) {
            assert ex.message == "Your test methods are annotated with dependency that are invalid. Check @Test(dependsOnMethods = ... )"
        }
    }

    void test_shouldSortTheDependencies() {
        def methodOne = new TestMethod(name: "one", dependsOn: "two")
        def methodTwo = new TestMethod(name: "two")
        def methodThree = new TestMethod(name: "three", dependsOn: "four")
        def methodFour = new TestMethod(name: "four")

        def methods = [methodOne, methodTwo, methodThree, methodFour]

        def sorted = sorter.sort(methods)

        assert sorted == [methodTwo, methodFour, methodOne, methodThree]
    }

    void test_shouldPutOneClassBeforeTheOther() {
        def methodOne = new TestMethod(name: "one", dependsOn: "two")
        def methodTwo = new TestMethod(name: "two")
        def methods = [methodOne, methodTwo]

        def sorted = sorter.sort(methods)

        assert sorted == [methodTwo, methodOne]
    }

    void test_shouldLeaveMethodsUnsortedWhenNoDependencyBetweenThem() {
        def methods = [new TestMethod(name: "one"), new TestMethod(name: "two"), new TestMethod(name: "three")]

        def sorted = sorter.sort(methods)

        assert sorted == methods
    }
}
