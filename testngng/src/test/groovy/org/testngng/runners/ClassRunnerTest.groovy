package org.testngng.runners

import org.testngng.fixtures.ClassWithTestAnnotatedOnClassItselfTest
import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.reporters.ClassResultXmlReporter
import org.testngng.domain.*

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 22/05/2012
 */
class ClassRunnerTest extends GroovyTestCase {

    void test_shouldFailAllTheTestsIfBeforeClassFailsWithException() {
        def wasCalled = false
        def testClass = new TestClass(beforeClassMethods: ['notExisting'], name: 'SomeClass', testMethods: ['methods'], instance: {return ""} )
        def singleTestRunner = [run:{instance, test-> wasCalled = true}] as TestRunner

        def runner = new ClassRunner(singleTestRunner: singleTestRunner)
        def result = runner.run(testClass)

        assert !wasCalled
        assert result.classResult == Result.FAILURE
    }

    void test_shouldCreateXmlReportWhenReporterIsSet() {
        def executed = false
        def singleTestRunner = [run: {instance, method -> Thread.sleep(10); TestResult.SUCCESS }] as TestRunner
        def xmlReporter = [createReportFileForClassResult: {results -> executed = true}] as ClassResultXmlReporter
        def runner = new ClassRunner(singleTestRunner: singleTestRunner, classResultXmlReporter: xmlReporter)
        def testClass = [name: "NotARealClass", instance: {return ""}] as TestClass

        testClass.testMethods = [new TestMethod(name: "doOne"), new TestMethod(name: "doTwo")]

        runner.run(testClass)

        assert executed
    }

    void test_shouldContainClassTimingInformation() {
        def singleTestRunner = [run: {instance, method -> Thread.sleep(10); TestResult.SUCCESS }] as TestRunner
        def runner = new ClassRunner(singleTestRunner: singleTestRunner)

        def testClass = [name: "NotARealClass", instance: {return ""}] as TestClass
        testClass.testMethods = [new TestMethod(name: "doOne"), new TestMethod(name: "doTwo")]

        def result = runner.run(testClass)

        assert result.time >= 1
    }

    void test_shouldSortTestMethodsIfThoseContainDependencies() {
        def providedInstance = new ClassWithTestAnnotation()
        def methodsInOrder = []
        def testMethod1 = new TestMethod(name: "testMethod1", dependsOn: "testMethod2")
        def testMethod2 = new TestMethod(name: "testMethod2")

        def singleTestRunner = [run: {instance, method -> methodsInOrder << method; return TestResult.success("yeahIRock")}] as TestRunner
        def sorter = [sort: {list -> return [testMethod2, testMethod1]}] as TestMethodsSorter

        def runner = new ClassRunner(singleTestRunner: singleTestRunner, testMethodsSorter: sorter)
        def testClass = [
                name: ClassWithTestAnnotation.name,
                instance: providedInstance,
                beforeClassMethods: [],
                afterClassMethods: [],
                testMethods: [testMethod1, testMethod2]
        ] as TestClass

        runner.run(testClass)

        assert methodsInOrder == [testMethod2, testMethod1]
    }

    void test_shouldReportFailureIfOneTestFailedWithinAClass() {
        def providedInstance = new ClassWithTestAnnotation()
        def singleTestRunner = [run: {instance, method -> return TestResult.failure("failed", "failed", null)}] as TestRunner

        def runner = new ClassRunner(singleTestRunner: singleTestRunner)
        def testClass = [
                name: ClassWithTestAnnotation.name,
                instance: providedInstance,
                beforeClassMethods: [],
                afterClassMethods: []
        ] as TestClass
        testClass.testMethods = [new TestMethod(name: "shouldTestFoo")]

        def result = runner.run(testClass)

        assert result.classResult == Result.FAILURE
    }

    void test_shouldExecuteAfterClassWhenAfterRunningAllTests() {
        def providedInstance = new ClassWithTestAnnotatedOnClassItselfTest()
        def singleTestRunner = [run: {instance, method -> return TestResult.success("")}] as TestRunner

        def runner = new ClassRunner(singleTestRunner: singleTestRunner)
        def testClass = [
                name: ClassWithTestAnnotatedOnClassItselfTest.name,
                instance: providedInstance
        ] as TestClass
        testClass.testMethods = [new TestMethod(name: "shouldTestFoo")]
        testClass.afterClassMethods = ["doAfterClass"]

        assert providedInstance.variable == ""

        runner.run(testClass)

        assert providedInstance.variable == "doAfterClass"
    }

    void test_shouldExecuteBeforeClassMethodBeforeRunningClassTest() {
        def providedInstance = new ClassWithTestAnnotatedOnClassItselfTest()
        def singleTestRunner = [run: {instance, method -> return TestResult.success("")}] as TestRunner

        def runner = new ClassRunner(singleTestRunner: singleTestRunner)
        def testClass = new TestClass(
                testMethods: [new TestMethod(name: "whateva")],
                name: ClassWithTestAnnotatedOnClassItselfTest.name,
                beforeClassMethods: ["doBeforeClass"], instance: providedInstance)

        runner.run(testClass)

        assert providedInstance.variable == "doBeforeClass"
    }

    void test_shouldUseTestRunnerToExecuteTestMethods() {
        def singleTestRunner = [run: {instance, method -> TestResult.SUCCESS }] as TestRunner
        def runner = new ClassRunner(singleTestRunner: singleTestRunner)

        def testClass = [name: "NotARealClass", instance: {return ""}] as TestClass
        testClass.testMethods = [new TestMethod(name: "doOne"), new TestMethod(name: "doTwo")]

        def result = runner.run(testClass)

        assert result.testResults.size() == 2
        assert result.classResult == Result.SUCCESS
        assert result.className == "NotARealClass"
        result.testResults.each { assert it == TestResult.SUCCESS }
    }

    void test_resultShouldContainNameOfExecutedClassAndTimeStamp() {
        def singleTestRunner = [:] as TestRunner
        def runner = new ClassRunner(singleTestRunner: singleTestRunner)
        def testClass = new TestClass(testMethods: [], name: ClassWithTestAnnotation.name)

        ClassResult classResult = runner.run(testClass)

        assert classResult.className == ClassWithTestAnnotation.name
        assert classResult.timestamp != 0
    }

}
