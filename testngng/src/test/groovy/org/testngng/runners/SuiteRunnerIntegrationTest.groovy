package org.testngng.runners

import org.testngng.assemblers.SuiteAssembler
import org.testngng.assemblers.TestClassAssembler
import org.testngng.converters.AnnotatedMethodToTestMethodConverter
import org.testngng.finders.TestAnnotationFinder
import org.testngng.finders.TestSetupMethodsFinder
import org.testngng.fixtures.ClassWithTestAnnotation
import org.testngng.fixtures.TestClassExtendingFromBaseClass
import org.testngng.fixtures.TestWithExpectedException
import org.testngng.fixtures.TestWithSimpleDataProvider
import org.testngng.fixtures.successfulSuite.TestClassOne
import org.testngng.fixtures.successfulSuite.TestClassTwo
import org.testngng.domain.*

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 04/06/2012
 */
class SuiteRunnerIntegrationTest extends GroovyTestCase {
    def assembler = new SuiteAssembler(
            testSetupMethodsFinder: new TestSetupMethodsFinder(),
            testClassAssembler: new TestClassAssembler(
                    methodConverter: new AnnotatedMethodToTestMethodConverter(),
                    testAnnotationFinder: new TestAnnotationFinder()
            )
    )

    def suiteRunner = new SuiteRunner(classRunner: new ClassRunner(singleTestRunner: new TestRunner(), testMethodsSorter: new TestMethodsSorter()))

    void test_shouldRunSuiteWithFailingTests() {
        def suite = buildSuiteWithClasses([TestWithSimpleDataProvider, TestWithExpectedException, TestClassExtendingFromBaseClass, TestClassOne, TestClassTwo])

        SuiteResult suiteResult = suiteRunner.run(suite)

        assert suiteResult.suiteResult == Result.FAILURE
        assert suiteResult.classResults.findAll {it.classResult == Result.FAILURE}.size() == 1
    }

    void test_shouldRunSuiteWithDataProviders() {
        def suite = buildSuiteWithClass(TestWithSimpleDataProvider)

        SuiteResult suiteResult = suiteRunner.run(suite)

        assert suiteResult.suiteResult == Result.SUCCESS
    }

    void test_shouldExecuteSuiteWithOneTestClass() {
        TestSuite suite = buildSuiteWithClass(ClassWithTestAnnotation)

        SuiteResult suiteResults = suiteRunner.run(suite)

        assert suiteResults.classResults.size() == 1
        assert suiteResults.classResults[0] instanceof ClassResult

        ClassResult classResult = suiteResults.classResults[0]

        assert classResult.testResults.size() == 4

        assert classResult.testResults.findAll {it == TestResult.SUCCESS}.size() == 2
        assert classResult.testResults.findAll {it == TestResult.FAILURE}.size() == 1
        assert classResult.testResults.findAll {it == TestResult.SKIPPED}.size() == 1

        assert classResult.classResult == Result.FAILURE
    }

    private TestSuite buildSuiteWithClasses(def listOfClasses) {
        assembler.assembleSuite(listOfClasses)
    }

    private TestSuite buildSuiteWithClass(Class className) {
        assembler.assembleSuite([className], "TestSuiteWith-${className.name}")
    }

}
