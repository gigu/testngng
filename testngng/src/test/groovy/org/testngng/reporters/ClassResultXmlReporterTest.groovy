package org.testngng.reporters

import org.junit.After
import org.junit.Before
import org.testngng.domain.ClassResult
import org.testngng.domain.Result
import org.testngng.domain.TestResult
import org.testngng.utils.FileSystemHelper

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 10/07/2012
 */
class ClassResultXmlReporterTest extends GroovyTestCase {
    def timestamp, folder
    File reportFile

    void test_shouldContainTimestamp() {
        def parsed = new XmlParser().parse(reportFile)
        assert parsed.@timestamp == timestamp.toString()
    }

    void test_shouldCreateSuiteReport() {
        assert reportFile.exists()
    }

    @Before
    void setUp() {
        timestamp = System.currentTimeMillis()

        def successfulTestResult = TestResult.success("awesomeTestingMethod")
        def exception = new NumberFormatException("Number was wrong")
        def failingTestResult = TestResult.failure("failingTestMethod", "I'm failing as I sux", exception)
        def classResult = new ClassResult(className: "org.testngng.TheAwesomeTestClass", classResult: Result.FAILURE, testResults: [successfulTestResult, failingTestResult], timestamp: timestamp)

        folder = new File("./${FileSystemHelper.TEMP_TEST_FOLDER}/reports")
        folder.mkdirs()

        def executor = new ClassResultXmlReporter(reportsFolder: folder)

        executor.createReportFileForClassResult(classResult)

        reportFile = new File("./${FileSystemHelper.TEMP_TEST_FOLDER}/reports/TEST-org.testngng.TheAwesomeTestClass.xml")
    }

    @After
    void tearDown() {
        FileSystemHelper.cleanFolderRecursively(folder)
    }
}
