package org.testngng.gradlePlugin

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 03/07/2012
 */
class TestNGNGGradlePluginTest extends GroovyTestCase {

    void test_shouldOverrideTestTaskFromJavaPlugin() {
        Project project = projectWithPluginsPlease()

        assert project.tasks.testngng
    }

    private Project projectWithPluginsPlease() {
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'java'
        project.apply plugin: 'testngng'
        project
    }
}
