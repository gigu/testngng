package org.testngng.gradlePlugin

class StreamOutputTest extends GroovyTestCase {

    void test_cli(){
        def b = new CliBuilder(usage: 'testngng [options]')
        b.cf(args: 1, argName: 'path', 'Path to folder with compiled classes')
        b.ex(args: 1, argName:  'exclude', 'Semicolon separated (;) list of excluded folders')
        b.in(args: 1, argName: 'include', 'Semicolon separated (;) list of included folders')
        b.sn(args: 1, argName: 'suiteName', 'Name of a suite to execute')

        b.usage()
        def result = b.parse(['-cf', 'some-special-path', '-ex','integration;foo;bar','whateva'])
        println result.arguments()
    }

    void test_shouldSpitDoubleToOutput(){
        def originalStream = System.out

        def stringStream = new ByteArrayOutputStream()

        def newStream = new PrintStream(new OutputStream(){
            @Override
            void write(int b) {
                stringStream.write(b)
                originalStream.write(b)
            }
        }, true)

        System.out = newStream

        print "Foo bar"
        assert stringStream.toString() == "Foo bar"

    }
}
