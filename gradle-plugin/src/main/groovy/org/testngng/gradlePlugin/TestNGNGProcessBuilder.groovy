package org.testngng.gradlePlugin

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 09/07/2012
 */
class TestNGNGProcessBuilder {
    def classpath, mainClass = 'org.testngng.TestNGNG', parameters = [], xms = '64M', xmx = '128M', logger
    File workingDir = new File(".")

    Process build() {
        def arguments = []
        arguments << 'java'
        arguments << "-Xms${xms}"
        arguments << "-Xmx${xmx}"
        arguments << '-classpath'
        arguments << classpath
        arguments << mainClass
        arguments.addAll parameters

        def builder = new ProcessBuilder(arguments as String[])
        builder.directory(workingDir)

        log "Classpath: $classpath"
        log "Main Class : $mainClass"
        log "Program parameters: $parameters"
        log "Xmx $xmx Xms $xms"
        builder.start()
    }

    private void log(String string) {
        logger.debug "TestNGNG -> $string"
    }
}
