package org.testngng.gradlePlugin

import org.gradle.api.Project
import org.gradle.api.GradleException

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 03/07/2012
 */
class TestNGNGTestTask {
    def executeTests(def project) {
        def classPath = getClassPath(project)
        def xmlReportsFolder = xmlReportsFolder(project)
        def htmlReportsFolder = htmlReportsFolder(project)
        def folderWithCompiledTestClasses = project.sourceSets.test.output.classesDir

        if (!project.extensions.testngng.containsSuiteDefinitions) {
            project.extensions.testngng.suites = [[name: 'GradleSuite', include:[]]]
        }
        executeMultipleSuites(folderWithCompiledTestClasses, classPath, xmlReportsFolder, htmlReportsFolder, project)
    }

    private def executeMultipleSuites(def folderWithCompiledTestClasses, def classPath, def xmlReportsFolder,
                                      def htmlReportsFolder, def project) {
        def success = true
        project.extensions.testngng.suites.each { def suite ->

            project.logger.lifecycle "Executing Suite $suite.name"
            def baseFolder = baseFolderForSuite(folderWithCompiledTestClasses, suite)
            def includeFolders = includeFolders(suite)
            Process process = new TestNGNGProcessBuilder(
                    classpath: classPath,
                    parameters: [
                            baseFolder,
                            xmlReportsFolder.absolutePath,
                            htmlReportsFolder.absolutePath,
                            suite.name,
                            includeFolders
                    ],
                    workingDir: project.buildDir,
                    logger: project.logger,
                    xmx: getXmx(project.extensions.testngng),
                    xms: getXms(project.extensions.testngng)
            ).build()

            def errorStream = new StringBuffer()

            consumeStandardOutputStream(process)
            process.consumeProcessErrorStream(errorStream)
            process.waitFor()

            print errorStream.toString()

            success = success && (process.exitValue() == 0)
        }
        if (!success) {
             throw new GradleException("Some tests have failed. Look in the $htmlReportsFolder.absolutePath for detailed report")
        }
    }

    private def getXmx(def testngng) {
        testngng?.xmx ?: '128M'
    }

    private def getXms(def testngng) {
        testngng?.xms ?: '64M'
    }

    private void consumeStandardOutputStream(Process process) {
        def inputStream = process.inputStream

        new Thread(new Runnable() {
            void run() {
                try {
                    def consume = true
                    byte[] buf = new byte[4068]
                    int read
                    while (consume) {
                        read = inputStream.read(buf);

                        if (read > 0) {
                            System.out.write(buf, 0, read);
                            System.out.flush();
                        }

                        consume = isAlive(process)
                    }
                } finally {
                    println ""
                }
            }
        }).start()
    }

    private boolean isAlive(Process process) {
        try {
            process.exitValue()
        } catch (exception) { return true }
        false
    }

    private def includeFolders(def suite) {
        suite.include ? suite.include.join(';') : ''
    }

    private def baseFolderForSuite(def baseFolder, def suite) {
        suite.folder ? new File(baseFolder, suite.folder) : baseFolder
    }

    private def htmlReportsFolder(Project project) {
        def folder = new File(project.buildDir, "reports/tests")
        folder.mkdirs()
        folder
    }

    private def xmlReportsFolder(Project project) {
        def folder = new File(project.buildDir, "test-results")
        folder.mkdirs()
        folder
    }

    private def getClassPath(Project project) {
        def classPath = []
        def separator = System.getProperty('path.separator')
        project.configurations.testRuntime.each { classPath << it }
        project.buildscript.configurations.classpath.each { classPath << it}
        project.sourceSets.each { classPath << it.output.classesDir }
        classPath.join(separator)
    }
}
