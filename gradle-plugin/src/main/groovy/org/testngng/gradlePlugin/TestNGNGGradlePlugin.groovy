package org.testngng.gradlePlugin

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * Created by Grzegorz (Greg) Gigon for TestNGNG
 * Date: 03/07/2012
 */
class TestNGNGGradlePlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.extensions.create('testngng', TestNGNGPluginExtenssion)
        project.task(
                'testngng',
                dependsOn: 'testClasses',
                description: "Runs TestNG test suite with TestNGNG runner",
                group: 'Verification') << {
            new TestNGNGTestTask().executeTests(project)
        }
    }

}
