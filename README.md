TestNG NG

This project is aiming at providing simple and deterministic way of executng tests annotated with TestNG annotations.

It's main goal is to make sure that all tests in a single class are executed together, without switching between test classes.

More about this project could be find on mine blog post: http://greggigon.com/2012/07/17/testng-ng-next-generation-of-a-runner-for-testng/